How to compile the program: 
1. please run the file through visual studio 2017
2. use platform toolset 141
3. run in debug
3. run in x86
4. MUST HAVE THRIFT AND BOOST INSTALLED ON THE 'C' DRIVE. CONFIGURED FOR THE FOLLOWING PATHS:
	- C:\Boost\boost_1_59_0\
	- C:\Thrift\thrift-0.9.1\

How to start the program:
1. Use Highscores.sql to copy the database onto your machine
2. Launch the GameServer First
3. Launch YOBM client

How to use the program:
1. The arrow keys will allow you to move the main character around the scene
2. The spacebar will make the main character fire a ball/missile
3. HIGHSCORE CONTROLS:
	F1 - Display top 20 scores
	D Key to activate Highscore entry prompt on the GLFW console screen, 
			Enter player Id when prompted,
			Enter highscore when prompted
	A key Get Highscore from local SQLLite
			Enter player Id when prompted

NOTES:
1. If player Id is already in the Database it will be updated, otherwise it is added
2. LUA plays sounds on weapon fire, player hit and background music.



