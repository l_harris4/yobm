
Object Start
normals: true
type: player
physics: false
wireframe: false
filename: BoxCowboyPly.ply
position: 9.2,-10.0,-4.8
scale: 1.3
colour: 0.3,1,0.3
Object End

Object Start
normals: true
type: banditR
physics: false
wireframe: false
filename: BoxBandit1.ply
position: -7.4,-9.8,-3.6
orientation: 1,-62,1
scale: 1
colour: 1,0.5,0
Object End

Object Start
normals: true
type: banditM
physics: false
wireframe: false
filename: BoxBandit2.ply
position: -2.6,-10,8.4
orientation: -3,57,1
scale: 1
colour: 0.7,0.5,0
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: crate.ply
position: 8.5,-8.7,8.5
orientation: 1,1,1
scale: 1
colour: 0.7,0.4,0.3
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: Pickaxe.ply
position: 6.6,-8.1,11.5
orientation: -94,106,109
scale: 0.5
colour: 0.7,0.4,0.3
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: -10.1,-9.9,12.4
orientation: -91,-21,8
scale: 0.5
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: 13.9,-9.9,16.9
orientation: -91,-23,-5
scale: 0.5
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: 17.5,-9.9,-7.1
orientation: -91,-13,2
scale: 0.5
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: -17,-9.9,12.1
orientation: -91,-13,2
scale: 0.5
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: -0.79,-9.9,-30.8
orientation: -91,-13,2
scale: 0.5
colour: 0.1,0.1,0.1
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: latern.ply
position: -2,-9.9,22
orientation: -91,-13,2
scale: 0.5
colour: 0.1,0.1,0.1
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: Rock1.ply
position: 0.99,-11.1,-1.7
orientation: -91,-23,-5
scale: 0.4
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: LogPly.ply
position: 0.99,-9.9,1.23
orientation: -91,-7,-5
scale: 0.8
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: LogPly.ply
position: 0.39,-9.9,-1.1
orientation: -91,149,-5
scale: 0.8
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: LogPly.ply
position: -0.21,-9.9,0.7
orientation: -91,253,5
scale: 0.8
colour: 0.1,0.1,0.1
Object End

Object Start
normals: true
type: object
physics: false
wireframe: false
filename: BushWithNoFruits.ply
position: -23.9,-9.9,-7.4
orientation: -91,-23,-5
scale: 2
colour: 0.5,0.5,0.1
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: cactus.ply
position: -13.2,-10.5,10.6
orientation: -86,153,-2
scale: 1.2
colour: 0.1,1,0.1
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: crate.ply
position: 10.9,-8.7,7.9
orientation: 1,31,1
scale: 1
colour: 0.7,0.4,0.3
Object End


Object Start
normals: true
type: object
physics: false
wireframe: false
filename: crate.ply
position: 9.7,-6.7,8.5
orientation: 1,31,1
scale: 1
colour: 0.7,0.4,0.3
Object End


Object Start
normals: false
type: object
physics: false
wireframe: false
filename: Flat_XZ_Plane_xyz.ply
position: 0,-10,0
orientation: 0,0,0
scale: 2
colour: 0.9,0.5,0.3
Object End

Light Start
//latern
position_l: 3.89,20.1,1.79
colour_l: 1,0.5,0
attentuation: 0,0.000001,0
Light End

Light Start
//latern
position_l: -10.5,-9.3,12
colour_l: 1,0.5,0
attentuation: 0,0.07,0
Light End

