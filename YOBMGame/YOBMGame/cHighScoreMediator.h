#ifndef CHIGHSCOREMEDIATOR_HG
#define CHIGHSCOREMEDIATOR_HG


#include "sqlite3.h"
#include <string>
#include <iostream>

const std::string DB_FILENAME = "exampledb.db";

const int MAX_SQL_LENGTH = 200;

/*
*  Name:		cHighScoreMediator
*  Summary:		Used to interact with the local sqllite database
*  Impliments:	nothing
*/
class cHighScoreMediator {
public:
	//ctor
	cHighScoreMediator();
	//dtor
	~cHighScoreMediator();
	//database instance
	sqlite3* db;
	/*
	*  Name:		OpenSQLiteDB
	*  Parameters:	a string representing the name of the db
	*  Summary:		creates the database instance
	*  Returns:		a bool saying if this succeeded or not
	*/
	bool OpenSQLiteDB(const std::string& filename);

	/*
	*  Name:		CheckSQLiteErrors
	*  Parameters:	an int representing a possible error code from sqllite
	*  Summary:		checks if an error has occurred
	*  Returns:		an int saying if it succeeded or not
	*/
	int CheckSQLiteErrors(int result);

	/*
	*  Name:		CloseSQLiteDB
	*  Parameters:	nothing
	*  Summary:		closes the connection to the sqllite database
	*  Returns:		nothing
	*/
	void CloseSQLiteDB();

	/*
	*  Name:		ExecuteSQLite
	*  Parameters:	a char* that represents the sql to be run
	*  Summary:		executes sql on the local database
	*  Returns:		nothing
	*/
	void ExecuteSQLite(char* sql);

	/*
	*  Name:		SetUpTable
	*  Parameters:	nothing
	*  Summary:		creates the table in the database
	*  Returns:		nothing
	*/
	void SetUpTable();

	/*
	*  Name:		SetHighScore
	*  Parameters:	an int representing the id of the row to be updated, and an int respresenting the new high score
	*  Summary:		sets a highscore in the db
	*  Returns:		nothing
	*/
	void SetHighScore(int id, int score);

	/*
	*  Name:		GetHighScore
	*  Parameters:	an int representing the id of the row/user we will to find the high score of
	*  Summary:		prints out the high score of a single row
	*  Returns:		nothing
	*/
	void GetHighScore(int id);
};


#endif // !CHIGHSCOREMEDIATOR_HG