#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Utilities.h"
#include "ModelUtilities.h"
#include "JsonUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cPhysicsManager.h"
#include "cPhysicsListener.h"
#include "cHighScoreMediator.h"
#include <set>
#include "cSoundEngine.h"
#include "cLuaBrain.h"
#include "cLightManager.h"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <boost\shared_ptr.hpp>
#include "..\dev\gen-cpp\Leaderboard.h"

using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

// Forward declaration of some functions
void DrawObject(cGameObject* pTheGO);
void PhysicsStep(double deltaTime);
bool LoadModelsLightsFromFile();
void LoadAllSounds();
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error);
void LoadPhysicsInterpretations();
bool IsShiftPressed(int mods);
bool IsCtrlPressed(int mods);
bool IsAltPressed(int mods);
bool IsShiftPressedAlone(int mods);
bool IsCtrlPressedAlone(int mods);
bool IsAltPressedAlone(int mods);
void InitializeThriftClient();
void SetHighScore(int player_id, int player_score);
void GetTop20Scores();

// The global container of game objects, utilized in the Render and Physics Steps
std::vector< cGameObject* >  g_vecGameObjects;

//The global container of souns
std::vector< cSoundEngine* > g_vecSoundObjects;

//Lua
cLuaBrain* p_LuaScripts = new cLuaBrain();

// Container for garbage object cleanup
std::set< cGameObject* > ballsScheduledForRemoval;

// *********************************************************
// Globals
std::string g_mExternalFile = "scene.json"; // External File definition
glm::vec3 g_cameraXYZ = glm::vec3(0.0f, 0.0f, 0.0f); // Camera World Position
glm::vec3 g_cameraTarget_XYZ = glm::vec3(0.0f, 0.0f, 0.0f); // Camera LookAt target
int g_selectedGameObjectIndex = 0; // GameObject Current Index
int g_selectedLightIndex = 0; // Light Current Index
bool g_movingGameObject = false; // boolean for editing GameObject positions 
bool g_lightsOn = true; // boolean for rendering lights
bool g_movingLights = false; // boolean for editiing Light positions
const float MOVESPEED = 0.3f; // Edit Object Position Speed
const float ROTATIONSPEED = -2; // Edit Object Orientation Speed
const float CAMERASPEED = 0.2f; // Camera Movement Speed
const float PLAYERSPEED = 7.5f; // Player Movement Speed

cVAOMeshManager*	g_pVAOManager = 0; // Vertex Array Object Manager - Loads Meshes into Memory
cShaderManager*		g_pShaderManager; // Shader Manager - Loads and Compiles shaders
cLightManager*		g_pLightManager; // Light Manager - Loads, Configures and Contains Lights in the Scene
cPhysicsManager*    g_pPhysicsManager; // Physics Manager - Handles all Physics Updates and Interactions between GameObjects
cHighScoreMediator* g_pHighScoreMediator;

LeaderboardClient*  g_clientLB; // Thrift connection
boost::shared_ptr<TTransport> thrift_socket(new TSocket("127.0.0.1", 9090));
boost::shared_ptr<TTransport> thrift_transport(new TBufferedTransport(thrift_socket));
boost::shared_ptr<TProtocol> thrift_protocol(new TBinaryProtocol(thrift_transport));
//cPhysicsListener*   g_pPhysicsListener;

bool g_bDrawDebugLightSpheres = false;

// ********************
// Shader Uniforms
GLint g_materialDiffuseLocation = -1; // Texture coloring uniform
GLint g_materialAmbientLocation = -1; // Ambient effect location uniform
GLint g_materialAmbientDiffuseRatioLocation = -1; // Ambient ratio uniform
GLint g_materialSpecularLocation = -1; // Specular value uniform
GLint g_isDebugWireFrameObject = -1; // Debug Boolean uniform

GLint g_eyePosition = -1; // Camera position
GLint g_model = -1; // World Matrix - Model
GLint g_view = -1; // World Matrix - View
GLint g_projection = -1; // World Matrix - Projection
// *********************************************************

/*
*  Name:		RotatePoint()
*  Parameters:	2 floats being the pivot point, 1 float being the angle, and 2 floats being the point to rotate
*  Summary:		rotates one point around a pivot point
*  Returns:		nothing
*/
void RotatePoint(float c1, float c2, float angle, float &point1, float &point2)
{
	// Convert angle into radians 
	angle = glm::radians(angle);

	// Calculation for point1
	float rotated1 = cos(angle) * (point1 - c1) - sin(angle) * (point2 - c2) + c1;

	// Calculation for point2
	float rotated2 = sin(angle) * (point1 - c1) + cos(angle) * (point2 - c2) + c2;

	// assign new values
	point1 = rotated1;
	point2 = rotated2;
}

/*
*  Name:		ErrorCallback()
*  Parameters:	an int being the error code, and const char* being the description of the error
*  Summary:		prints an error description to screen
*  Returns:		nothing
*/
static void ErrorCallback(int error, const char* description)
{
	// Print Error to Console
	fprintf(stderr, "Error: %s\n", description);
}


/*
*  Name:		KeyCallback()
*  Parameters:	a GLFWwindow* to the window, an int being the key pressed, an int being the scancode,
*				an int being the action, and an int being the mods
*  Summary:		handles key input
*  Returns:		nothing
*/
static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//if the key pressed was escape
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		// End Application Loop - Close Window
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}// end if

	// Get Top 20 Scores
	if (key == GLFW_KEY_F1 && action == GLFW_PRESS)
	{
		GetTop20Scores();
	}// end if

	//if the key was pressed or is being held down
	if (action == GLFW_PRESS || action == GLFW_REPEAT)
	{
		switch (key)
		{
			//switching between which object is "selected"
		case GLFW_KEY_U:
			// Increment current GameObject index
			::g_selectedGameObjectIndex++;

			//if the selected object index is not valid
			if (::g_selectedGameObjectIndex >= ::g_vecGameObjects.size())
			{
				// Set to zero
				::g_selectedGameObjectIndex = 0;
			}// end if

			// Increment current GameObject index
			::g_selectedLightIndex++;

			//if the selected light index is not valid
			if (::g_selectedLightIndex >= ::g_pLightManager->vecLights.size())
			{
				// Set to zero
				::g_selectedLightIndex = 0;
			}// end if
			break;

			//Toggle between controlling light/object/camera
		case GLFW_KEY_I:
			//if a game object is selected
			if (::g_movingGameObject)
			{
				// controlling lights
				::g_movingGameObject = false;
				::g_movingLights = true;
			}// end if
			//if a light is selected
			else if (::g_movingLights)
			{
				// no longer controlling Lights
				::g_movingLights = false;
			}// end if
			else
			{
				// controlling GameObjects
				::g_movingGameObject = true;
			}// end else
			break;

			// Move an object or light negative in x axis, or rotate camera negative in x axis
		case GLFW_KEY_A:
			//take in an id for get high score
		{
			std::cout << "Please input an id to search for:";
			int tempId;
			std::cin >> tempId;
			::g_pHighScoreMediator->GetHighScore(tempId);
		}
		break;
		case GLFW_KEY_D:
		{
			std::cout << "Please input an id for the high score you are going to update:";
			int tempId;
			std::cin >> tempId;
			std::cout << "Please input the high score you wish for them to have";
			int highScore;
			std::cin >> highScore;
			::g_pHighScoreMediator->SetHighScore(tempId,highScore);
			SetHighScore(tempId, highScore); // Thrift Set HighScore
		}
		break;
		case GLFW_KEY_W:
			//if moving a game object 
			if (::g_movingGameObject)
			{
				// Move selected object negatively along the z axis 
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z -= MOVESPEED;
			}// end if
			//if moving a light
			else if (::g_movingLights)
			{
				// Move selected light negatively along the z axis 
				::g_pLightManager->vecLights[g_selectedLightIndex].position.z -= MOVESPEED;

				//if the light being moved has a game object attached to it
				if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
				{
					// Retrieve the Lights GameObject index
					int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;

					// Move the lights object negatively along the z axis 
					::g_vecGameObjects[localIndex]->position.z -= MOVESPEED;
				}// end if
			}// end if
			else
			{
				// Move camera positively along the y axis
				g_cameraXYZ.y += CAMERASPEED;
				g_cameraTarget_XYZ.y += CAMERASPEED;
			}// end else
			break;

			// Move an object or light positive in z axis, or rotate camera positive in z axis
		case GLFW_KEY_S:
			//if moving a game object
			if (::g_movingGameObject)
			{
				// Move selected object positively along the z axis 
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z += MOVESPEED;
			}// end if
			//if moving the lights
			else if (::g_movingLights)
			{
				// Move selected light positively along the z axis 
				::g_pLightManager->vecLights[g_selectedLightIndex].position.z += MOVESPEED;

				//if the light has a game object attached to it
				if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
				{
					// Retrieve GameObject index from selected light
					int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;

					// Move the lights object positively along the z axis 
					::g_vecGameObjects[localIndex]->position.z += MOVESPEED;
				}// end if
			}// end if
			else
			{
				// Move Camera negatively along the y axis 
				g_cameraXYZ.y -= CAMERASPEED;
				g_cameraTarget_XYZ.y -= CAMERASPEED;
			}// end else
			break;
			// Move an object or light negative in y axis, or rotate camera negative in y axis
		case GLFW_KEY_Q:
			//if moving a game object
			if (::g_movingGameObject)
			{
				// Move selected object negatively along the y axis 
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y -= MOVESPEED;
			}// end else
			//if moving a light
			else if (::g_movingLights)
			{
				// Move selected light negatively along the y axis 
				::g_pLightManager->vecLights[g_selectedLightIndex].position.y -= MOVESPEED;

				//if the light has a game object attached to it
				if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
				{
					// Retrieve GameObject index from selected light
					int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;

					// Move the lights object negatively along the y axis 
					::g_vecGameObjects[localIndex]->position.y -= MOVESPEED;
				}// end if
			}
			else
			{
				// Rotate Camera negatively along the y axis
				RotatePoint(::g_cameraXYZ.z, ::g_cameraXYZ.y, -ROTATIONSPEED, ::g_cameraTarget_XYZ.z, ::g_cameraTarget_XYZ.y);
			}// end else
			break;

			// Move an object or light positive in y axis, or rotate camera positive in y axis
		case GLFW_KEY_E:
			//if moving a game object
			if (::g_movingGameObject)
			{
				// Move selected object positively along the y axis 
				::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y += MOVESPEED;
			}// end if
			//if moving a light
			else if (::g_movingLights)
			{
				// Move selected light positively along the y axis 
				::g_pLightManager->vecLights[g_selectedLightIndex].position.y += MOVESPEED;

				//if the light has a game object attached to it
				if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
				{
					// Retrieve GameObject index from selected index
					int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;

					// Move the lights object positively along the y axis 
					::g_vecGameObjects[localIndex]->position.y += MOVESPEED;
				}// end if
			}// end if
			else
			{
				// Rotate Camera Positively along the y axis
				RotatePoint(::g_cameraXYZ.z, ::g_cameraXYZ.y, ROTATIONSPEED, ::g_cameraTarget_XYZ.z, ::g_cameraTarget_XYZ.y);
			}// end else
			break;

			//used for debugging, printing the current location of object
		case GLFW_KEY_P:
			//if a game object is currently selected
			if (::g_movingGameObject)
			{
				// Output Object position
				std::cout << "Object location:" << std::endl;
				printf("%.2f,%.2f,%.2f\n",
					::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x,
					::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y,
					::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z);

				// Output Object orientation
				std::cout << "Object orientation:" << std::endl;
				printf("%.2f,%.2f,%.2f\n",
					glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x),
					glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y),
					glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z));
			}// end if
			//if a light is selected
			else if (::g_movingLights)
			{
				// Output Light position
				std::cout << "Light location:" << std::endl;
				printf("%.2f,%.2f,%.2f\n",
					::g_pLightManager->vecLights[::g_selectedLightIndex].position.x,
					::g_pLightManager->vecLights[::g_selectedLightIndex].position.y,
					::g_pLightManager->vecLights[::g_selectedLightIndex].position.z);
			}// end if
			else
			{
				// Output Camera position
				std::cout << "Camera: " << std::endl;
				printf("%.2f,%.2f,%.2f\n",
					g_cameraXYZ.x,
					g_cameraXYZ.y,
					g_cameraXYZ.z);

				// Output Camera Target position
				std::cout << "Camera Target" << std::endl;
				printf("%.2f,%.2f,%.2f\n",
					g_cameraTarget_XYZ.x,
					g_cameraTarget_XYZ.y,
					g_cameraTarget_XYZ.z);
			}// end else
			break;

			//PerformAction for the player "fire weapon"
		case GLFW_KEY_SPACE:
			// Call Perform action on the player GameObject
			::g_vecGameObjects[0]->PerformAction();
			break;

			//Move the player "upward"
		case GLFW_KEY_UP:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(oldVel.x, -PLAYERSPEED));
		}
		break;

		//Move the player "downward"
		case GLFW_KEY_DOWN:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(oldVel.x, PLAYERSPEED));
		}
		break;

		//Move the player "leftward"
		case GLFW_KEY_LEFT:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(-PLAYERSPEED, oldVel.y));
		}
		break;

		//Move the player "rightward"
		case GLFW_KEY_RIGHT:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(PLAYERSPEED, oldVel.y));
		}
		break;

		//switching between wireframe or not
		case GLFW_KEY_O:
			// Toggle selected GameObject isWireframe variable
			::g_vecGameObjects[::g_selectedGameObjectIndex]->isWireFrame =
				!::g_vecGameObjects[::g_selectedGameObjectIndex]->isWireFrame;
			break;

			//1 - 6 are used to change the rotation of an object
		case GLFW_KEY_1:
			// Roatate selected GameObject Positively X axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x) + 1);
			break;
		case GLFW_KEY_2:
			// Roatate selected GameObject Negatively X axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.x) - 1);
			break;
		case GLFW_KEY_3:
			// Roatate selected GameObject Positively Y axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y) + 1);
			break;
		case GLFW_KEY_4:
			// Roatate selected GameObject Negatively Y axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.y) - 1);
			break;
		case GLFW_KEY_5:
			// Roatate selected GameObject Positively Z axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z) + 1);
			break;
		case GLFW_KEY_6:
			// Roatate selected GameObject Negatively Z axis
			::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z =
				glm::radians(glm::degrees(::g_vecGameObjects[::g_selectedGameObjectIndex]->orientation2.z) - 1);
			break;

			//Move the camera "forward"
		case GLFW_KEY_Z:
		{
			// Calculation of distance to move between Camera Position and Target
			glm::vec3 movementVector(g_cameraTarget_XYZ.x - g_cameraXYZ.x, g_cameraTarget_XYZ.y - g_cameraXYZ.y, g_cameraTarget_XYZ.z - g_cameraXYZ.z);

			// Multiply movement vector by the movement speed to get new position
			glm::vec3 endVector = ::g_cameraXYZ + glm::vec3(CAMERASPEED*movementVector.x, CAMERASPEED*movementVector.y, CAMERASPEED*movementVector.z);

			// Offset the new positions for the camera target
			g_cameraTarget_XYZ.x += (endVector.x - g_cameraXYZ.x);
			g_cameraTarget_XYZ.y += (endVector.y - g_cameraXYZ.y);
			g_cameraTarget_XYZ.z += (endVector.z - g_cameraXYZ.z);

			// Set camera to the new position
			g_cameraXYZ = endVector;
		}
		break;

		//Move the camera "backward"
		case GLFW_KEY_X:
		{
			// Calculation of distance to move between Camera Position and Target
			glm::vec3 movementVector(g_cameraTarget_XYZ.x - g_cameraXYZ.x, g_cameraTarget_XYZ.y - g_cameraXYZ.y, g_cameraTarget_XYZ.z - g_cameraXYZ.z);

			// Multiply movement vector by the movement speed to get new position
			glm::vec3 endVector = ::g_cameraXYZ + glm::vec3(-CAMERASPEED*movementVector.x, -CAMERASPEED*movementVector.y, -CAMERASPEED*movementVector.z);

			// Offset the new positions for the camera target
			g_cameraTarget_XYZ.x += (endVector.x - g_cameraXYZ.x);
			g_cameraTarget_XYZ.y += (endVector.y - g_cameraXYZ.y);
			g_cameraTarget_XYZ.z += (endVector.z - g_cameraXYZ.z);

			// Set camera to the new position
			g_cameraXYZ = endVector;
		}
		break;
		}
	}// end if
	else if (action == GLFW_PRESS)
	{
	}
	else
	{
		switch (key)
		{
			//Stop moving the player "upward"
		case GLFW_KEY_UP:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(oldVel.x, 0));
		}
		break;
		//Stop moving the player "downward"
		case GLFW_KEY_DOWN:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(oldVel.x, 0));
		}
		break;
		//Stop moving the player "leftward"
		case GLFW_KEY_LEFT:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(0, oldVel.y));
		}
		break;
		//Stop moving the player "rightward"
		case GLFW_KEY_RIGHT:
		{
			// Get Player Velocity 
			b2Vec2 oldVel = ::g_vecGameObjects[0]->physicsBody->GetLinearVelocity();

			// Modify and Set Player Velocity
			::g_vecGameObjects[0]->physicsBody->SetLinearVelocity(b2Vec2(0, oldVel.y));
			//::g_vecGameObjects[0]->vel.x = 0;
		}
		}

	}// end else

	return;
}

int main(void)
{
	// MVP Uniform Location
	GLint mvp_location;

	LoadAllSounds();

	// GLFW window
	GLFWwindow* window;

	// GLFW set error callback
	glfwSetErrorCallback(ErrorCallback);

	// Initialize GLFW
	if (!glfwInit())
		exit(EXIT_FAILURE); // Exit if Initialize Fails

	// Default Window Height
	int height = 480;

	// Default Window Width
	int width = 640;

	// Default Window Title
	std::string title = "Default Title";

	// Read window config file
	std::ifstream infoFile("config.txt");

	// if the file didn't open
	if (!infoFile.is_open())
	{
		// Output failure string
		std::cout << "Can't find config file" << std::endl;
		std::cout << "Using defaults" << std::endl;
	}// end if
	else
	{
		// read tokens specific to config file
		std::string a;
		infoFile >> a;
		infoFile >> a;
		infoFile >> a;
		infoFile >> width; // retrieve width
		infoFile >> a;
		infoFile >> height; // retrieve height
		infoFile >> a;

		// title stream for pushing tokens
		std::stringstream ssTitle;

		// Initialize read loop boolean
		bool bKeepReading = true;
		do
		{
			infoFile >> a;
			//if the title is not title end
			if (a != "Title_End")
			{
				// append token to string stream
				ssTitle << a << " ";
			}// end if
			else
			{
				// stop the loop
				bKeepReading = false;

				// set title with read in string stream
				title = ssTitle.str();
			}// end else
		} while (bKeepReading);


	}//end if

	// Thrift connection
	InitializeThriftClient();

	// Set GLFW Window Version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// Initialize Window with default or loaded properties
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);

	// If window failed to initialize
	if (!window)
	{
		// Shutdown GLFW
		glfwTerminate();

		// Exit application
		exit(EXIT_FAILURE);
	}

	// Set keyboard callback
	glfwSetKeyCallback(window, KeyCallback);

	// Switch focus/context from console to glfw window
	glfwMakeContextCurrent(window);

	// Load OpenGL into GLFW window
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	// Output OpenGL Version and Hardware Data
	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// Initialize Physics Manager
	::g_pPhysicsManager = new cPhysicsManager();
	//::g_pPhysicsListener = new cPhysicsListener();
	::g_pPhysicsManager->SetContactListener(new cPhysicsListener());

	::g_pHighScoreMediator = new cHighScoreMediator();
	//::g_pHighScoreMediator->SetUpTable();
	//::g_pHighScoreMediator->SetHighScore(1, 10);
	//::g_pHighScoreMediator->SetHighScore(2, 20);
	//::g_pHighScoreMediator->SetHighScore(3, 30);
	//::g_pHighScoreMediator->GetHighScore(3);
	//::g_pHighScoreMediator->SetHighScore(3, 20);
	//::g_pHighScoreMediator->GetHighScore(3);
	//::g_pHighScoreMediator->GetHighScores(1);
	//::g_pHighScoreMediator->GetHighScores(2);
	//::g_pHighScoreMediator->GetHighScores(3);

	// Initialize Shader Manager
	::g_pShaderManager = new cShaderManager();

	// Create Shader Objects
	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	// Set shader files
	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	// Set base path to shader files
	::g_pShaderManager->SetBasePath("assets//shaders//");

	// Create Shader Program
	if (!::g_pShaderManager->CreateProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		// If Failure when loading shaders
		// Print Error
		std::cout << "Error with the shader" << std::endl;
		std::cout << ::g_pShaderManager->GetLastError() << std::endl;
		// exit application
		return -1;
	}
	std::cout << "The shaders compiled and linked OK" << std::endl;


	// Initialize VAOManager
	::g_pVAOManager = new cVAOMeshManager();

	// Get ShaderID
	GLint sexyShaderID = ::g_pShaderManager->GetIDFromFriendlyName("mySexyShader");

	// string errors will be sent to
	std::string error;

	// Load ply files into VAO
	if (!LoadModelFilesFromJSON(::g_mExternalFile, sexyShaderID, ::g_pVAOManager, error))
	{
		// If failed to load Models into VAO
		// Print errors
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}

	// Load Camera Settings from Json File
	LoadCameraFromJSON(::g_mExternalFile, ::g_cameraXYZ, ::g_cameraTarget_XYZ);

	// Load Models into scene from Json File
	LoadGameObjectsFromJSON(::g_mExternalFile);

	//go through each object and attach a physics object to the appropriate ones
	LoadPhysicsInterpretations();

	// Get ShaderID
	GLint currentProgID = ::g_pShaderManager->GetIDFromFriendlyName("mySexyShader");

	// Get the uniform locations for this shader
	mvp_location = glGetUniformLocation(currentProgID, "MVP");
	g_materialDiffuseLocation = glGetUniformLocation(currentProgID, "materialDiffuse");
	g_materialAmbientLocation = glGetUniformLocation(currentProgID, "materialAmbient");
	g_materialAmbientDiffuseRatioLocation = glGetUniformLocation(currentProgID, "ambientToDiffuseRatio");
	g_materialSpecularLocation = glGetUniformLocation(currentProgID, "materialSpecular");
	g_isDebugWireFrameObject = glGetUniformLocation(currentProgID, "bIsDebugWireFrameObject");
	g_eyePosition = glGetUniformLocation(currentProgID, "eyePosition");
	g_model = glGetUniformLocation(currentProgID, "mModel");
	g_view = glGetUniformLocation(currentProgID, "mView");
	g_projection = glGetUniformLocation(currentProgID, "mProjection");

	// Initialize Light Manger
	::g_pLightManager = new cLightManager();

	// Retrieve Number of Lights to Create
	int numberOfLights;
	GetNumLightsFromJSON(::g_mExternalFile, numberOfLights);

	// Create Lights
	::g_pLightManager->CreateLights(numberOfLights);

	// Load Uniform Locations in LightManager
	::g_pLightManager->LoadShaderUniformLocations(currentProgID);

	// Set Light Properties
	LoadLightsFromJSON(::g_mExternalFile, ::g_pLightManager->vecLights);

	// Enable Depth Render
	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();

	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		// Aspect Ratio
		float ratio;

		// Window width and height
		int width, height;

		// World Matrix
		glm::mat4x4 p, mvp;

		// GLFW Window Buffer
		glfwGetFramebufferSize(window, &width, &height);

		// Calculate Aspect Ratio
		ratio = width / (float)height;

		// Set Viewport
		glViewport(0, 0, width, height);

		// Clear colour AND depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Retrieve shader program
		::g_pShaderManager->UseShaderProgram("mySexyShader");

		// Retrieve shaderID
		GLint shaderID = ::g_pShaderManager->GetIDFromFriendlyName("mySexyShader");

		// Update all the light uniforms...
		// (for the whole scene)
		::g_pLightManager->CopyLightInformationToCurrentShader();

		// Projection and view don't change per scene (maybe)
		p = glm::perspective(0.6f,
			ratio,
			0.1f,
			1000.0f);

		// View or "camera" matrix
		glm::mat4 v = glm::mat4(1.0f);

		// Set viewport
		v = glm::lookAt(g_cameraXYZ,
			g_cameraTarget_XYZ,
			glm::vec3(0.0f, 1.0f, 0.0f));

		// Pass viewport to shader
		glUniformMatrix4fv(g_view, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(v));

		// Pass projection to shader
		glUniformMatrix4fv(g_projection, 1, GL_FALSE,
			(const GLfloat*)glm::value_ptr(p));

		// Retrieve number of GameObjects to Render
		unsigned int sizeOfVector = ::g_vecGameObjects.size();

		// Render the scene
		for (int index = 0; index != sizeOfVector; index++)
		{
			cGameObject* pTheGO = ::g_vecGameObjects[index];

			DrawObject(pTheGO);

		}// end for

		// Retrieve current time in GLFW application life
		double curTime = glfwGetTime();

		// Calculate deltatime for physics and game updates
		double deltaTime = curTime - lastTimeStep;

		// Perform Physics Step
		PhysicsStep(deltaTime);


		// Save time of update for next loop deltatime calculation
		lastTimeStep = curTime;

		// Reveal the Buffer to the window
		glfwSwapBuffers(window);
		glfwPollEvents();

	}// end while

	// GLFW shutdown
	glfwDestroyWindow(window);
	glfwTerminate();

	// Cleanup
	delete ::g_pShaderManager;
	delete ::g_pVAOManager;
	delete ::g_pLightManager;
	//delete ::g_pPhysicsListener;
	delete ::g_pPhysicsManager;
	delete ::g_clientLB;

	return 0;
}

/*
*  Name:		PhysicsStep()
*  Parameters:	double for the time
*  Summary:		updates the all the object in the scene
*  Returns:		nothing
*/
void PhysicsStep(double deltaTime)
{
	// Call the Physics Manager Update, pass deltatime
	::g_pPhysicsManager->Update(deltaTime);

	//loop through all the objects in the scene
	for (int index = 0; index != ::g_vecGameObjects.size(); index++)
	{
		// Retrieve pointer to current GameObject
		cGameObject* pCurGO = ::g_vecGameObjects[index];

		// Call Update specific to Object
		pCurGO->Update(deltaTime);
	}// end for

	return;
}

/*
*  Name:		DrawObject()
*  Parameters:	a game object pointer being the current object
*  Summary:		draws a single object to screen
*  Returns:		nothing
*/
void DrawObject(cGameObject* pTheGO)
{
	// Ignore Invalid Objects
	if (pTheGO == 0)
	{
		return;
	}

	// Retrieve MeshName
	std::string meshToDraw = pTheGO->meshName;

	// Retrieve VAO Data
	sVAOInfo VAODrawInfo;
	if (::g_pVAOManager->LookupVAOFromName(meshToDraw, VAODrawInfo) == false)
	{
		return;
	}

	// World Matrix Model - Identity
	glm::mat4x4 mModel = glm::mat4x4(1.0f);

	// Z azis Pre-Rotation Matrix
	glm::mat4 matRreRotZ = glm::mat4x4(1.0f);
	matRreRotZ = glm::rotate(matRreRotZ, pTheGO->orientation.z,
		glm::vec3(0.0f, 0.0f, 1.0f));
	mModel = mModel * matRreRotZ;

	// Translation Matrix
	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans,
		pTheGO->position);
	mModel = mModel * trans;

	// Z axis Post-Rotation Matrix
	glm::mat4 matPostRotZ = glm::mat4x4(1.0f);
	matPostRotZ = glm::rotate(matPostRotZ, pTheGO->orientation2.z,
		glm::vec3(0.0f, 0.0f, 1.0f));
	mModel = mModel * matPostRotZ;

	// Y axis Post-Rotation Matrix
	glm::mat4 matPostRotY = glm::mat4x4(1.0f);
	matPostRotY = glm::rotate(matPostRotY, pTheGO->orientation2.y,
		glm::vec3(0.0f, 1.0f, 0.0f));
	mModel = mModel * matPostRotY;

	// X axis Post-Rotation Matrix
	glm::mat4 matPostRotX = glm::mat4x4(1.0f);
	matPostRotX = glm::rotate(matPostRotX, pTheGO->orientation2.x,
		glm::vec3(1.0f, 0.0f, 0.0f));
	mModel = mModel * matPostRotX;

	// Retrieve GameObject Scale
	float finalScale = pTheGO->scale;

	// Scale Transform Matrix
	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(finalScale,
			finalScale,
			finalScale));
	mModel = mModel * matScale;

	// Save model matrix to GameObject
	pTheGO->mModel = mModel;

	// Send Model Matrix to shader
	glUniformMatrix4fv(g_model, 1, GL_FALSE,
		(const GLfloat*)glm::value_ptr(mModel));

	// TODO: Later use with physics
	glm::mat4 mWorldInTranpose = glm::inverse(glm::transpose(mModel));

	// Send Diffuse Color to shader
	glUniform4f(g_materialDiffuseLocation,
		pTheGO->diffuseColour.r,
		pTheGO->diffuseColour.g,
		pTheGO->diffuseColour.b,
		pTheGO->diffuseColour.a);

	//if the object is wireframe
	if (pTheGO->isWireFrame)
	{
		// Send boolean to shader
		glUniform1f(g_isDebugWireFrameObject, 1.0f);

		// Set shader to draw line 
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		// Disable Cull face
		glDisable(GL_CULL_FACE);
	}// end if
	else
	{
		// Send boolean to shader
		glUniform1f(g_isDebugWireFrameObject, 0.0f);

		// Set Shader to draw fill
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// Enable Depth
		glEnable(GL_DEPTH_TEST);

		// Enable Cull Face 
		glEnable(GL_CULL_FACE);
	}// end else

	// Set Cull face to back
	glCullFace(GL_BACK);

	// Bind VAO to Draw
	glBindVertexArray(VAODrawInfo.VaoID);

	// Draw the VAO
	glDrawElements(GL_TRIANGLES,
		VAODrawInfo.numberOfIndices,
		GL_UNSIGNED_INT,
		0);

	// Remove Bind to VAO
	glBindVertexArray(0);

	return;
}

/*
*  Name:		LoadPhysicsInterpretations()
*  Parameters:	void
*  Summary:		Initializes Box2D Physics Behaviours in the scene/level
*  Returns:		nothing
*/
void LoadPhysicsInterpretations() {
	//YOBM (Player)
	{
		// BodyDef
		b2BodyDef sphereBodyDef;

		// Position
		sphereBodyDef.position.Set(g_vecGameObjects[0]->position.x, g_vecGameObjects[0]->position.z);

		// Type 
		sphereBodyDef.type = b2_dynamicBody;						// This allows the object to move

		// Initialize
		g_vecGameObjects[0]->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);

		// Assign
		g_vecGameObjects[0]->physicsBody->SetUserData(g_vecGameObjects[0]);

		// Shape
		b2CircleShape circleShape;

		// Position
		circleShape.m_p.Set(0.0f, 0.0f);

		// Size
		circleShape.m_radius = 1.3;

		// Create
		g_vecGameObjects[0]->physicsBody->CreateFixture(&circleShape, 1.0f);
	}

	//bandit1
	{
		// BodyDef
		b2BodyDef sphereBodyDef;

		// Position
		sphereBodyDef.position.Set(g_vecGameObjects[1]->position.x, g_vecGameObjects[1]->position.z);

		// Type
		sphereBodyDef.type = b2_dynamicBody;						// This allows the object to move

		// Initialize
		g_vecGameObjects[1]->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);

		// Assign
		g_vecGameObjects[1]->physicsBody->SetUserData(g_vecGameObjects[1]);

		// Shape
		b2CircleShape circleShape;

		// Position
		circleShape.m_p.Set(0.0f, 0.0f);

		// Size
		circleShape.m_radius = 1.3;

		// Create
		g_vecGameObjects[1]->physicsBody->CreateFixture(&circleShape, 1.0f);
	}

	//bandit2
	{
		// BodyDef
		b2BodyDef sphereBodyDef;

		// Position
		sphereBodyDef.position.Set(g_vecGameObjects[2]->position.x, g_vecGameObjects[2]->position.z);

		// Type
		sphereBodyDef.type = b2_dynamicBody;						// This allows the object to move

		// Initialize
		g_vecGameObjects[2]->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);

		// Assign
		g_vecGameObjects[2]->physicsBody->SetUserData(g_vecGameObjects[2]);

		// Shape
		b2CircleShape circleShape;

		// Position
		circleShape.m_p.Set(0.0f, 0.0f);

		// Size
		circleShape.m_radius = 1.3;

		// Create
		g_vecGameObjects[2]->physicsBody->CreateFixture(&circleShape, 1.0f);
	}

	int gameObjectsSize = ::g_vecGameObjects.size();
	//RIGHT WALL
	{
		// BodyDef
		b2BodyDef platformBodyDef;

		// Position
		platformBodyDef.position.Set(g_vecGameObjects[gameObjectsSize - 4]->position.x, g_vecGameObjects[gameObjectsSize - 4]->position.z);

		// Initialize
		g_vecGameObjects[gameObjectsSize - 4]->physicsBody = ::g_pPhysicsManager->CreateBody(&platformBodyDef);

		// Assign
		g_vecGameObjects[gameObjectsSize - 4]->physicsBody->SetUserData(g_vecGameObjects[gameObjectsSize - 4]);

		// Shape
		b2PolygonShape platformShape;

		// Size
		platformShape.SetAsBox(10, 100);

		// Create
		g_vecGameObjects[gameObjectsSize - 4]->physicsBody->CreateFixture(&platformShape, 0.0f);
	}

	//LEFT WALL
	{
		// BodyDef
		b2BodyDef platformBodyDef;

		// Position
		platformBodyDef.position.Set(g_vecGameObjects[gameObjectsSize - 3]->position.x, g_vecGameObjects[gameObjectsSize - 3]->position.z);

		// Initialize
		g_vecGameObjects[gameObjectsSize - 3]->physicsBody = ::g_pPhysicsManager->CreateBody(&platformBodyDef);

		// Assign
		g_vecGameObjects[gameObjectsSize - 3]->physicsBody->SetUserData(g_vecGameObjects[gameObjectsSize - 3]);

		// Shape
		b2PolygonShape platformShape;

		// Size
		platformShape.SetAsBox(10, 100);

		// Create
		g_vecGameObjects[gameObjectsSize - 3]->physicsBody->CreateFixture(&platformShape, 0.0f);
	}

	//Bottom WALL
	{
		// BoduDef
		b2BodyDef platformBodyDef;

		// Position
		platformBodyDef.position.Set(g_vecGameObjects[gameObjectsSize - 2]->position.x, g_vecGameObjects[gameObjectsSize - 2]->position.z);

		// Initialize
		g_vecGameObjects[gameObjectsSize - 2]->physicsBody = ::g_pPhysicsManager->CreateBody(&platformBodyDef);

		// Assign
		g_vecGameObjects[gameObjectsSize - 2]->physicsBody->SetUserData(g_vecGameObjects[gameObjectsSize - 2]);

		// Shape
		b2PolygonShape platformShape;

		// Size
		platformShape.SetAsBox(100, 10);

		// Create
		g_vecGameObjects[gameObjectsSize - 2]->physicsBody->CreateFixture(&platformShape, 0.0f);
	}

	//TOP WALL
	{
		// BodyDef
		b2BodyDef platformBodyDef;

		// Position
		platformBodyDef.position.Set(g_vecGameObjects[gameObjectsSize - 1]->position.x, g_vecGameObjects[gameObjectsSize - 1]->position.z);

		// Initialize
		g_vecGameObjects[gameObjectsSize - 1]->physicsBody = ::g_pPhysicsManager->CreateBody(&platformBodyDef);

		// Assign
		g_vecGameObjects[gameObjectsSize - 1]->physicsBody->SetUserData(g_vecGameObjects[gameObjectsSize - 1]);

		// Shape
		b2PolygonShape platformShape;

		// Size
		platformShape.SetAsBox(100, 10);

		// Create
		g_vecGameObjects[gameObjectsSize - 1]->physicsBody->CreateFixture(&platformShape, 0.0f);
	}

	//go through a loop and make enemies and rocks and the such
}

/*
*  Name:		IsShiftPressed()
*  Parameters:	an int being the mods
*  Summary:		determines if shift is pressed or not
*  Returns:		a bool saying if it was pressed or not
*/
bool IsShiftPressed(int mods)
{
	//if shift is pressed
	if (mods & GLFW_MOD_SHIFT)
	{
		return true;
	}// end if
	return false;
}

/*
*  Name:		IsCtrlPressed()
*  Parameters:	an int being the mods
*  Summary:		determines if ctrl is pressed or not
*  Returns:		a bool saying if it was pressed or not
*/
bool IsCtrlPressed(int mods)
{
	//if control is pressed
	if (mods & GLFW_MOD_CONTROL)
	{
		return true;
	}// end if
	return false;
}

/*
*  Name:		IsAltPressed()
*  Parameters:	an int being the mods
*  Summary:		determines if alt is pressed or not
*  Returns:		a bool saying if it was pressed or not
*/
bool IsAltPressed(int mods)
{
	//if alt is pressed
	if (mods & GLFW_MOD_ALT)
	{
		return true;
	}// end if
	return false;
}

/*
*  Name:		IsShiftPressedAlone()
*  Parameters:	an int being the mods
*  Summary:		determines if alt and only alt
*  Returns:		a bool saying if it was pressed alone or not
*/
bool IsShiftPressedAlone(int mods)
{
	//if shift and only shift is pressed
	if ((mods & GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT)
	{
		return true;
	}// end if
	return false;
}

/*
*  Name:		IsCtrlPressedAlone()
*  Parameters:	an int being the mods
*  Summary:		determines if ctrl and only ctrl
*  Returns:		a bool saying if it was pressed alone or not
*/
bool IsCtrlPressedAlone(int mods)
{
	//if ctrl and only control is pressed
	if ((mods & GLFW_MOD_CONTROL) == GLFW_MOD_CONTROL)
	{
		return true;
	}// end if
	return false;
}

/*
*  Name:		IsAltPressedAlone()
*  Parameters:	an int being the mods
*  Summary:		determines if alt and only alt
*  Returns:		a bool saying if it was pressed alone or not
*/
bool IsAltPressedAlone(int mods)
{
	//if alt and alt alone is pressed
	if ((mods & GLFW_MOD_ALT) == GLFW_MOD_ALT)
	{
		return true;
	}// end if
	return false;
}

void InitializeWSA()
{
	int iResult = -1;
	WSADATA wsaData;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		exit(-1);
	}
}

/*
*  Name:		InitializeThriftClient()
*  Parameters:	void
*  Summary:		Initializes the Thrift client with the socket information
*  Returns:		a bool saying if it was pressed alone or not
*/
void InitializeThriftClient()
{	
	::g_clientLB = new LeaderboardClient(thrift_protocol);

	InitializeWSA();
}

void SetHighScore(int player_id, int player_score)
{
	try {
		thrift_transport->open();

		// Set HighScore
		/*int32_t playerId = 1;
		int32_t playerScore = 200;*/
		::g_clientLB->setHighScore(player_id, player_score);

		thrift_transport->close();
	}
	catch (TException& tx) {
		cout << "ERROR: " << tx.what() << endl;
	}
}

void GetTop20Scores()
{
	try {
		thrift_transport->open();

		// Retrieve Top 20 scores
		std::map<int32_t, int32_t> mapTop20;
		::g_clientLB->getTop20(mapTop20);

		thrift_transport->close();

		std::cout << "-------------------------" << std::endl;
		std::cout << "       HIGHSCORES        " << std::endl;
		std::cout << "-------------------------" << std::endl;
		for (std::map<int32_t, int32_t>::iterator it = mapTop20.begin(); it != mapTop20.end(); it++)
		{
			std::cout << "Player: " << it->first << ", Score: " << it->second << std::endl;
		}
		std::cout << "\nSaving to local db..." << std::endl;
		for (std::map<int32_t, int32_t>::iterator it = mapTop20.begin(); it != mapTop20.end(); it++)
		{
			::g_pHighScoreMediator->SetHighScore(it->first, it->second); // Save locally
		}
	}
	catch (TException& tx) {
		cout << "ERROR: " << tx.what() << endl;
	}
}

