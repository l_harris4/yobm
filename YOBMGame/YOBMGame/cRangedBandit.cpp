#include "cRangedBandit.h"
#include <vector>
#include <glm\trigonometric.hpp>
#include <glm\vec2.hpp>
#include <glm\vec4.hpp>
#include "cBulletPool.h"
#include "cPhysicsManager.h"
#include "cLuaBrain.h"
#include "cSoundEngine.h"
#include <sstream>

//the container of all sound objects
extern std::vector< cSoundEngine* > g_vecSoundObjects;
//Lua
extern cLuaBrain* p_LuaScripts;
//a container of all the game objects
extern std::vector< cGameObject* >  g_vecGameObjects;
//the physics manager for the whole world
extern cPhysicsManager* g_pPhysicsManager;


//ctor
cRangedBandit::cRangedBandit()
{
	//if the bandit is stunned or not
	this->stunned = false;
	//the time that a bandit can be stunned for
	this->stunTime = 50;
	this->stunTimeReset = this->stunTime;
	//if the bandit is facing the player or not
	this->facingPlayer = false;
	//a timer that determines when the bandit shoots
	this->shootTimer = 100;
	this->shootTimerReset = this->shootTimer;
}

//dtor
cRangedBandit::~cRangedBandit()
{
}

/*
*  Name:		calcDistance
*  Parameters:	2 glm::vec2 of the 2 2D points
*  Summary:		calculates the distance between 2 points
*  Returns:		the distance between the 2 points
*/
float calcDistance(glm::vec2 point1, glm::vec2 point2)
{
	return glm::distance(point1, point2);
}

/*
*  Name:		Contact
*  Parameters:	a cGameObject pointer of the game object this object
*				has made contact with
*  Summary:		a method that is called when this object
*				makes contact with another object
*  Returns:		nothing
*/
void cRangedBandit::Contact(cGameObject * otherObject)
{
	stunned = true;
	this->originalColour = this->diffuseColour;
	this->diffuseColour = glm::vec4(1, 0, 0, 1);
}

/*
*  Name:		Update()
*  Parameters:	double for the time
*  Summary:		Controls the bandits behavior
*  Returns:		nothing
*/
void cRangedBandit::Update(double deltaTime)
{
	//the position of the object
	b2Vec2 physicsPosition;
	//if the bandit has a physics body or not
	if (physicsBody != 0)
	{
		//if the bandit is stunned or not
		if (stunned)
		{
			//Lua Script Callback
			::p_LuaScripts->SetObjectVector(&(::g_vecSoundObjects));

			std::stringstream ssGotHit;

			std::string soundName = "death";
			ssGotHit << "RangedHitByYOBM('";
			ssGotHit << soundName;
			ssGotHit << "') \n";

			std::cout << ssGotHit.str() << std::endl;

			::p_LuaScripts->LoadScript("RangedHit", ssGotHit.str());


			//Update Lua script
			::p_LuaScripts->Update();


			physicsBody->SetLinearVelocity(b2Vec2(0,0));
			stunTime -= 1;
			//if the bandit should be stunned
			if (stunTime < 0)
			{
				this->diffuseColour = this->originalColour;
				stunned = false;
				stunTime = stunTimeReset;
			}// end if
		}// end if
		else
		{
			//if the player is close to the bandit
			if (glm::distance(glm::vec2(g_vecGameObjects[0]->position.x,
				g_vecGameObjects[0]->position.z),
				glm::vec2(position.x, position.z)) < 15)
			{
				//make the bandit not face the player
				facingPlayer = false;
				b2Vec2 directionVector = b2Vec2(g_vecGameObjects[0]->position.x - position.x,
					g_vecGameObjects[0]->position.z - position.z);
				directionVector.Normalize();

				float modificationValue = 5;
				directionVector = b2Vec2(directionVector.x* modificationValue, directionVector.y * modificationValue);

				//set the velocity of the bandit
				physicsBody->SetLinearVelocity(-directionVector);


				physicsPosition = physicsBody->GetPosition();
				position.x = physicsPosition.x;
				position.z = physicsPosition.y;

				//set the orientation of the bandit
				b2Vec2 velocity = physicsBody->GetLinearVelocity();
				//if the bandit has a velocity
				if (velocity.x != 0 || velocity.y != 0)
				{
					float angle = OrientAngle(glm::vec2(velocity.x, velocity.y));
					orientation2.y = glm::radians(glm::degrees(angle) - 90.0f);
				}// end if
			}// end if
			else
			{
				//set the bandit to facing the player
				facingPlayer = true;
				//set the direction vector of the 
				b2Vec2 directionVector = b2Vec2(g_vecGameObjects[0]->position.x - position.x,
					g_vecGameObjects[0]->position.z - position.z);
				directionVector.Normalize();

				physicsBody->SetLinearVelocity(b2Vec2(0, 0));

				//move the angle toward facing the player
				float angle = OrientAngle(glm::vec2(directionVector.x, directionVector.y));
				float finalAngle = glm::radians(glm::degrees(angle) - 90.0f);
				//set the angle of the of the orientation the bandit
				//if the angle isn't correct
				if (!(finalAngle > (orientation2.y - 0.1) && finalAngle < (orientation2.y + 0.1)))
				{
					orientation2.y += (finalAngle - orientation2.y)*0.3;
				}// end if
			}// end else
		}// end else
	}// end if


	//if the bandit is facing the player
	if (facingPlayer)
	{
		shootTimer--;
		//if bandit should be shooting
		if (shootTimer <= 0)
		{
			shootTimer = shootTimerReset;
			Fire();
		}// end if
	}// end if
}

/*
*  Name:		Fire
*  Parameters:	nothing
*  Summary:		makes the bandit fire a bullet
*  Returns:		nothing
*/
void cRangedBandit::Fire()
{
	//get the velocity of the bullet using the orientation of the bandit
	b2Vec2 tempVel(cos(orientation2.y), sin(orientation2.y));
	tempVel.Normalize();
	tempVel.y = -tempVel.y;


	//create a new game object and add it to the vector of objects
	// Create cGameObject using factory
	cGameObject* pTempGO = cBulletPool::GetInstance()->Acquire();

	//if the bullet doesnt have a physics body
	if (!pTempGO->physicsBody)
	{
		pTempGO->meshName = "sphere";
		pTempGO->scale = 0.3;
		pTempGO->isWireFrame = false;
		pTempGO->isUpdatedInPhysics = true;

		pTempGO->position = glm::vec3(position.x + (tempVel.x * 5), position.y + 3, position.z + (tempVel.y * 5));
		pTempGO->diffuseColour = this->diffuseColour;

		//create a physics object to be linked to that object
		b2BodyDef sphereBodyDef;
		sphereBodyDef.position.Set(pTempGO->position.x, pTempGO->position.z);
		sphereBodyDef.type = b2_dynamicBody;
		pTempGO->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);
		pTempGO->physicsBody->SetUserData(pTempGO);

		b2CircleShape circleShape;
		circleShape.m_p.Set(0.0f, 0.0f);
		circleShape.m_radius = pTempGO->scale;
		pTempGO->physicsBody->CreateFixture(&circleShape, 1.0f);

		int modificationValue = 20;
		tempVel = b2Vec2(tempVel.x * modificationValue, tempVel.y * modificationValue);
		pTempGO->physicsBody->SetLinearVelocity(tempVel);

		// Push the GameObject onto the global vector of game objects
		g_vecGameObjects.push_back(pTempGO);
	}// end if
	else
	{
		//destroy the old physics body
		pTempGO->physicsBody->GetWorld()->DestroyBody(pTempGO->physicsBody);

		// Need to Convert these elements to glm::vec3 and vec4s
		pTempGO->position = glm::vec3(position.x + (tempVel.x * 5), position.y + 3, position.z + (tempVel.y * 5));
		pTempGO->diffuseColour = this->diffuseColour;

		//create a physics object to be linked to that object
		b2BodyDef sphereBodyDef;
		sphereBodyDef.position.Set(pTempGO->position.x, pTempGO->position.z);
		sphereBodyDef.type = b2_dynamicBody;						
		pTempGO->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);
		pTempGO->physicsBody->SetUserData(pTempGO);

		b2CircleShape circleShape;
		circleShape.m_p.Set(0.0f, 0.0f);
		circleShape.m_radius = pTempGO->scale;
		pTempGO->physicsBody->CreateFixture(&circleShape, 1.0f);

		int modificationValue = 20;
		tempVel = b2Vec2(tempVel.x * modificationValue, tempVel.y * modificationValue);
		pTempGO->physicsBody->SetLinearVelocity(tempVel);
	}
}
