#ifndef _cVAOMeshManager_HG_
#define _cVAOMeshManager_HG_

#include <string>
#include "cTriangle.h"
#include <vector>

//forward declaration
class cMesh;

#include <map>

/*
*  Name:		sVAOInfo
*  Summary:		contains info needed for the Vertex Array Object VAO
*  Impliments:	nothing
*/
struct sVAOInfo
{
	//ctor
	sVAOInfo() : VaoID(0), shaderID(-1),
		vertexBufferID(0), indexBufferID(0),
		numberOfVertices(0), numberOfTriangles(0),
		numberOfIndices(0),
		scaleForUnitBBox(1.0f)
	{ }
	//data members
	// the friendly name of a mesh
	std::string friendlyName;

	// the id of the vao
	unsigned int VaoID;

	// the number of vertices in a mesh
	unsigned int numberOfVertices;

	// the number of triangles in a mesh
	unsigned int numberOfTriangles;

	// the number of indices in a mesh
	unsigned int numberOfIndices;

	// a vector containing all the triangles
	std::vector< cPhysTriangle > vecPhysTris;

	// the id of the shader that will display the mesh
	int shaderID;

	// the vertex buffer id
	unsigned int vertexBufferID;

	// the index buffer id
	unsigned int indexBufferID;

	// the scale of the mesh
	float scaleForUnitBBox;
};


/*
*  Name:		sVAOInfo
*  Summary:		loads a mesh into various buffers and keeps track of named VAO infos
*  Impliments:	nothing
*/
class cVAOMeshManager
{
public:
	//ctor
	cVAOMeshManager();
	//dtor
	~cVAOMeshManager();
	/*
	*  Name:		LoadMeshIntoVAO()
	*  Parameters:	a mesh object to be changed and an int representing the current shader
	*  Summary:		loads a mesh into a VAO object
	*  Returns:		a bool saying if it worked or not
	*/
	bool LoadMeshIntoVAO(cMesh &theMesh, int shaderID);

	/*
	*  Name:		LookupVAOFromName()
	*  Parameters:	string being the name of the mesh, and the VAOInfo object that we want
	*				the information to be contained int
	*  Summary:		finds the relavent vao object to a certain mesh
	*  Returns:		a bool sayig if it worked or not
	*/
	bool LookupVAOFromName(std::string name, sVAOInfo &theVAOInfo);

	// data members
	// a map used to get names out of numbers
	std::map<int, std::string> mapNumberToName;
private:
	// a map used to select the right vao by name
	std::map< std::string, sVAOInfo > mapNameToVAO;
};


#endif //!_cVAOMeshManager_HG_
