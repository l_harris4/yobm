#ifndef _cSoundEngine_HG_
#define _cSoundEngine_HG_

#include <fmod\fmod.hpp>
#include <fmod\fmod_errors.h>
#include <iostream>

class cSoundEngine
{
public:
	//constructor
	cSoundEngine();

	//destructor
	~cSoundEngine();

	//FMOD_RESULT variable for storing result of any action
	FMOD_RESULT result;

	//FMOD systerm variable
	FMOD::System *system;

	//FMOD sound variable
	FMOD::Sound *sound;

	//FMOD Channel variable
	FMOD::Channel *channel;

	//a string variable representing name of sound to be played
	std::string nameOfSound;

	//boolean variable to store pause status of a channel
	bool pauseStatus;

	/*
	*  Name:		getID()
	*  Parameters:	nothing
	*  Summary:		get a unique id for a sound object
	*  Returns:		nothing
	*/
	int getID(void);

	/*
	*  Name:		errorCheck()
	*  Parameters:	FMOD_RESULT
	*  Summary:		checks for any errors after performing sound related task
	*  Returns:		nothing
	*/
	void errorCheck(FMOD_RESULT result);

	/*
	*  Name:		createSound()
	*  Parameters:	name of a file as string
	*  Summary:		is a method to create a sound
	*  Returns:		nothing
	*/
	void createSound(std::string fileName);

	/*
	*  Name:		playSound()
	*  Parameters:	nothing
	*  Summary:		is a method to play a sound
	*  Returns:		nothing
	*/
	void playSound();

	/*
	*  Name:		pauseSound()
	*  Parameters:	boolean state
	*  Summary:		is a method to toggle pause state of a channel
	*  Returns:		nothing
	*/
	void pauseSound(bool pauseState);

private:
	//unique id for a sound object
	int m_uniqueID;

	//the next unique id to be assigned for new sound object
	static int m_nextID;
};
#endif // !_cSoundEngine_HG_
