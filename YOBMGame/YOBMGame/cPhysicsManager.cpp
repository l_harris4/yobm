#include "cPhysicsManager.h"
#include "cGameObject.h"
#include <vector>
#include <set>

//the bullets that will be put back into the 
extern std::set<cGameObject*> ballsScheduledForRemoval;
//a collection of all the game objects
extern std::vector< cGameObject* >  g_vecGameObjects;

//ctor
cPhysicsManager::cPhysicsManager()
	: mGravity(b2Vec2(0.0f, 0.0f))
	, mVelocityIterations(6)
	, mPositionIterations(2)
{
	mPhysicsWorld = new b2World(mGravity);
}

//dtor
cPhysicsManager::~cPhysicsManager()
{
}

/*
*  Name:		StartUp
*  Parameters:	nothing
*  Summary:		starts the physics simulation of the world
*  Returns:		nothing
*/
void cPhysicsManager::StartUp()
{
	mPhysicsWorld = new b2World(mGravity);
}

/*
*  Name:		Shutdown
*  Parameters:	nothing
*  Summary:		stops the physics simulation of the world
*  Returns:		nothing
*/
void cPhysicsManager::Shutdown()
{
}

/*
*  Name:		Update
*  Parameters:	passed time in a float
*  Summary:		Updates the objects in the world based on time
*				and velocity
*  Returns:		nothing
*/
void cPhysicsManager::Update(float dt)
{
	mPhysicsWorld->Step(dt, mVelocityIterations, mPositionIterations);

	//the following part of this method is work in progress code

	//This is for removing bullets, but it doesnt work currently, sometimes there is an exception thrown in
	//contact where something collides with a nullptr
	//std::set<cGameObject*>::iterator it = ballsScheduledForRemoval.begin();
	//std::set<cGameObject*>::iterator end = ballsScheduledForRemoval.end();
	//for (; it != end; ++it) {
	//	cGameObject* dyingBall = *it;

	//	//delete ball... physics body is destroyed here
	//	delete dyingBall;

	//	//... and remove it from main list of balls
	//	std::vector<cGameObject*>::iterator it = std::find(g_vecGameObjects.begin(), g_vecGameObjects.end(), dyingBall);
	//	if (it != g_vecGameObjects.end())
	//		g_vecGameObjects.erase(it);
	//}

	////clear this list for next time
	//ballsScheduledForRemoval.clear();

}

/*
*  Name:		SetGravity
*  Parameters:	vec2 representing gravity
*  Summary:		sets the gravity vector of the world
*  Returns:		nothing
*/
void cPhysicsManager::SetGravity(b2Vec2 gravity)
{
	mGravity = gravity;
}

/*
*  Name:		SetContactListener
*  Parameters:	a contact listener pointer
*  Summary:		sets the contact listener of the world
*  Returns:		nothing
*/
void cPhysicsManager::SetContactListener(b2ContactListener* contactListener)
{
	mPhysicsWorld->SetContactListener(contactListener);
}

/*
*  Name:		CreateBody
*  Parameters:	a const pointer to a body
*  Summary:		creates a physics object in the world
*  Returns:		returns a pointer to a physics body that it creates
*/
b2Body* cPhysicsManager::CreateBody(const b2BodyDef* def)
{
	return mPhysicsWorld->CreateBody(def);
}

/*
*  Name:		DestroyBody
*  Parameters:	a const pointer to a body
*  Summary:		removes a physics body from the world
*  Returns:		nothing
*/
void cPhysicsManager::DestroyBody(b2Body* body)
{
	mPhysicsWorld->DestroyBody(body);
}