#include "cLuaBrain.h"
#include <iostream>
#include "cSoundEngine.h"


extern std::vector< cSoundEngine* > g_vecSoundObjects;

/*static*/
std::vector< cSoundEngine* >* cLuaBrain::m_p_vecSoundEngineObjects;

//constuctor
cLuaBrain::cLuaBrain()
{
	this->m_p_vecSoundEngineObjects = nullptr;

	// Create new Lua state.
	this->m_pLuaState = luaL_newstate();

	luaL_openlibs(this->m_pLuaState);					/* Lua 5.3.3 */

	//Connects to lua script when YOBM fires a bullet
	lua_pushcfunction(this->m_pLuaState, cLuaBrain::l_FireBullets);
	lua_setglobal(this->m_pLuaState, "FireBullets");

	//Connects to lua script when Melee Bandit is hit by YOBM
	lua_pushcfunction(this->m_pLuaState, cLuaBrain::l_MeleeHitByYOBM);
	lua_setglobal(this->m_pLuaState, "MeleeHitByYOBM");

	//Connects to lua script when Ranged Bandit is hit by a bullet fired by YOBM
	lua_pushcfunction(this->m_pLuaState, cLuaBrain::l_RangedHitByYOBM);
	lua_setglobal(this->m_pLuaState, "RangedHitByYOBM");

	return;
}

//destructor
cLuaBrain::~cLuaBrain()
{
	lua_close(this->m_pLuaState);
	return;
}

/*
*  Name:		LoadScript()
*  Parameters:	name of the script and script source
*  Summary:		Load Lua script and assign a name to it
*  Returns:		nothing
*/
void cLuaBrain::LoadScript(std::string scriptName,
	std::string scriptSource)
{
	this->m_mapScripts[scriptName] = scriptSource;
	return;
}

/*
*  Name:		DeleteScript()
*  Parameters:	name of the script
*  Summary:		Delete script using its name
*  Returns:		nothing
*/
void cLuaBrain::DeleteScript(std::string scriptName)
{
	// TODO: delete this script
	return;
}

/*
*  Name:		SetObjectVector()
*  Parameters:	vector of SoundEngine objects
*  Summary:		Passes a pointer to the Sound object vector
*  Returns:		nothing
*/
void cLuaBrain::SetObjectVector(std::vector< cSoundEngine* >* p_vecSoundObjects)
{
	this->m_p_vecSoundEngineObjects = p_vecSoundObjects;
	return;
}

/*
*  Name:		Update()
*  Parameters:	name of the script
*  Summary:		Call all the active scripts that are loaded
*  Returns:		nothing
*/
void cLuaBrain::Update()
{

	for (std::map< std::string /*name*/, std::string /*source*/>::iterator itScript =
		this->m_mapScripts.begin(); itScript != this->m_mapScripts.end(); itScript++)
	{
		
		std::string curLuaScript = itScript->second;

		int error = luaL_loadstring(this->m_pLuaState,
			curLuaScript.c_str());

		if (error != 0 /*no error*/)
		{
			std::cout << "-------------------------------------------------------" << std::endl;
			std::cout << "Error running Lua script: ";
			std::cout << itScript->first << std::endl;
			std::cout << this->m_decodeLuaErrorToString(error) << std::endl;
			std::cout << "-------------------------------------------------------" << std::endl;
			continue;
		}

		error = lua_pcall(this->m_pLuaState,	/* lua state */
			0,	/* nargs: number of arguments pushed onto the lua stack */
			0,	/* nresults: number of results that should be on stack at end*/
			0);	/* errfunc: location, in stack, of error function.
				if 0, results are on top of stack. */
		if (error != 0 /*no error*/)
		{
			std::cout << "Lua: There was an error..." << std::endl;
			std::cout << this->m_decodeLuaErrorToString(error) << std::endl;

			std::string luaError;
			luaError.append(lua_tostring(this->m_pLuaState, -1));

			// Make error message a little more clear
			std::cout << "-------------------------------------------------------" << std::endl;
			std::cout << "Error running Lua script: ";
			std::cout << itScript->first << std::endl;
			std::cout << luaError << std::endl;
			std::cout << "-------------------------------------------------------" << std::endl;
			// We passed zero (0) as errfunc, so error is on stack)
			lua_pop(this->m_pLuaState, 1);  /* pop error message from the stack */

			continue;
		}
		
	}
	this->m_mapScripts.clear();
	return;
}




/*
*  Name:		l_FireBullets()
*  Parameters:	Lua State
*  Summary:		Plays bullet sound on active
*  Returns:		valid (true or false)
*/
/*static*/ int cLuaBrain::l_FireBullets(lua_State *L)
{
	std::string soundNameFromLua = lua_tostring(L, 1);	/* get argument */
	cSoundEngine* pSO;
	for (int i = 0; i != g_vecSoundObjects.size(); i++)
	{
		if (g_vecSoundObjects[i]->nameOfSound == soundNameFromLua)
		{
			pSO = g_vecSoundObjects[i];
			continue;
		}
		
	}


	if (pSO == nullptr)
	{	// No, it's invalid
		lua_pushboolean(L, false);
		return 1;
	}

	pSO->playSound();

	lua_pushboolean(L, true);	// index is OK

	return 1;		
}

/*
*  Name:		l_MeleeHitByYOBM()
*  Parameters:	Lua State
*  Summary:		Plays grunt sound on active
*  Returns:		valid (true or false)
*/
/*static*/ int cLuaBrain::l_MeleeHitByYOBM(lua_State *L)
{
	std::string soundNameFromLua = lua_tostring(L, 1);	/* get argument */
	cSoundEngine* pSO;
	for (int i = 0; i != g_vecSoundObjects.size(); i++)
	{
		if (g_vecSoundObjects[i]->nameOfSound == soundNameFromLua)
			pSO = g_vecSoundObjects[i];
	}

	if (pSO == nullptr)
	{	// No, it's invalid
		lua_pushboolean(L, false);
		return 1;
	}

	pSO->playSound();

	lua_pushboolean(L, true);	// index is OK

	return 1;
}


/*
*  Name:		l_RangedHitByYOBM()
*  Parameters:	Lua State
*  Summary:		Plays death sound on active
*  Returns:		valid (true or false)
*/
/*static*/ int cLuaBrain::l_RangedHitByYOBM(lua_State *L)
{
	std::string soundNameFromLua = lua_tostring(L, 1);	/* get argument */
	cSoundEngine* pSO;
	for (int i = 0; i != g_vecSoundObjects.size(); i++)
	{
		if (g_vecSoundObjects[i]->nameOfSound == soundNameFromLua)
			pSO = g_vecSoundObjects[i];
	}


	if (pSO == nullptr)
	{	// No, it's invalid
		lua_pushboolean(L, false);
		// I pushed 1 thing on stack, so return 1;
		return 1;
	}

	pSO->pauseSound(false);

	lua_pushboolean(L, true);	// index is OK

	return 1;
}


/*
*  Name:		m_decodeLuaErrorToString()
*  Parameters:	lua error code
*  Summary:		Decodes any Lua error to a readble error
*  Returns:		error in string format
*/
std::string cLuaBrain::m_decodeLuaErrorToString(int error)
{
	switch (error)
	{
	case 0:
		return "Lua: no error";
		break;
	case LUA_ERRSYNTAX:
		return "Lua: syntax error";
		break;
	case LUA_ERRMEM:
		return "Lua: memory allocation error";
		break;
	case LUA_ERRRUN:
		return "Lua: Runtime error";
		break;
	case LUA_ERRERR:
		return "Lua: Error while running the error handler function";
		break;
	}//switch ( error )

	return "Lua: UNKNOWN error";
}
