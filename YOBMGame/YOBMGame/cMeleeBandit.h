#ifndef _cMeleeBandit_HG_
#define _cMeleeBandit_HG_
#include "cGameObject.h"

/*
*  Name:		cMeleeBandit
*  Summary:		Represents a bandit that will try to attack Ye Olde Bazooka Man
*  Impliments:	cGameObject
*/
class cMeleeBandit : public cGameObject
{
public:
	//ctor
	cMeleeBandit();
	//dtor
	~cMeleeBandit();

	// whether or not the bandit is stunned
	bool stunned;

	// the time remaining of being stunned
	int stunTime;

	// the maximum amount of time the bandit can be stunned
	int stunTimeReset;

	/*
	*  Name:		Update
	*  Parameters:	double for the time
	*  Summary:		Controls the bandits behavior
	*  Returns:		nothing
	*/
	virtual void Update(double deltaTime);

	/*
	*  Name:		Contact
	*  Parameters:	a cGameObject pointer of the game object this object
	*				has made contact with
	*  Summary:		a method that is called when this object
	*				makes contact with another object
	*  Returns:		nothing
	*/
	virtual void Contact(cGameObject* otherObject);
};

#endif // !_cMeleeBandit_HG_
