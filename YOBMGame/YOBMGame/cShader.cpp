#include "cShaderManager.h"

//ctor
cShaderManager::cShader::cShader()
{
	this->ID = 0;
	this->shaderType = cShader::UNKNOWN;
	return;
}

//dtor
cShaderManager::cShader::~cShader()
{
	return;
}

/*
*  Name:		GetShaderTypeString()
*  Parameters:	nothing
*  Summary:		Retrieves the type of the shader
*  Returns:		a string representing the type of the shader
*/
std::string cShaderManager::cShader::GetShaderTypeString(void)
{
	switch (this->shaderType)
	{
	case cShader::VERTEX_SHADER:
		return "VERTEX_SHADER";
		break;
	case cShader::FRAGMENT_SHADER:
		return "FRAGMENT_SHADER";
		break;
	case cShader::UNKNOWN:
	default:
		return "UNKNOWN_SHADER_TYPE";
		break;
	}// end switch
	return "UNKNOWN_SHADER_TYPE";
}
