#ifndef _cLuaBrain_HG_
#define _cLuaBrain_HG_

extern "C" {
#include <lua\lua.h>
#include <lua\lua.h>
#include <lua\lauxlib.h>
#include <lua\lualib.h>
}

#include <string>
#include <vector>
#include "cSoundEngine.h"
#include <map>

class cLuaBrain
{
public:
	//constructor
	cLuaBrain();

	//destructor
	~cLuaBrain();

	/*
	*  Name:		LoadScript()
	*  Parameters:	name of the script and script source
	*  Summary:		Load Lua script and assign a name to it
	*  Returns:		nothing
	*/
	void LoadScript(std::string scriptName, std::string scriptSource);

	/*
	*  Name:		DeleteScript()
	*  Parameters:	name of the script
	*  Summary:		Delete script using its name
	*  Returns:		nothing
	*/
	void DeleteScript(std::string scriptName);
	

	/*
	*  Name:		SetObjectVector()
	*  Parameters:	vector of SoundEngine objects
	*  Summary:		Passes a pointer to the Sound object vector
	*  Returns:		nothing
	*/
	void SetObjectVector(std::vector< cSoundEngine* >* p_vecGOs);

	/*
	*  Name:		Update()
	*  Parameters:	name of the script
	*  Summary:		Call all the active scripts that are loaded
	*  Returns:		nothing
	*/
	void Update();

	/*
	*  Name:		l_FireBullets()
	*  Parameters:	Lua State
	*  Summary:		Plays bullet sound on active
	*  Returns:		valid (true or false)
	*/
	static int l_FireBullets(lua_State *L);

	/*
	*  Name:		l_MeleeHitByYOBM()
	*  Parameters:	Lua State
	*  Summary:		Plays grunt sound on active
	*  Returns:		valid (true or false)
	*/
	static int l_MeleeHitByYOBM(lua_State *L);
	
	/*
	*  Name:		l_RangedHitByYOBM()
	*  Parameters:	Lua State
	*  Summary:		Plays death sound on active
	*  Returns:		valid (true or false)
	*/
	static int l_RangedHitByYOBM(lua_State *L);


private:

	//Maps script name to a script source
	std::map< std::string /*scriptName*/, std::string /*scriptSource*/ > m_mapScripts;

	//member variable containing vector of SoundEngine objects
	static std::vector< cSoundEngine* >* m_p_vecSoundEngineObjects;

	//a pointer variable to lua_State
	lua_State* m_pLuaState;


	/*
	*  Name:		m_decodeLuaErrorToString()
	*  Parameters:	lua error code
	*  Summary:		Decodes any Lua error to a readble error
	*  Returns:		error in string format
	*/
	std::string m_decodeLuaErrorToString(int error);
};

#endif _cLuaBrain_HG_
