#ifndef _cVertex_HG_
#define _cVertex_HG_

/*
*  Name:		cVertex
*  Summary:		Represents a vertext of a triangle
*  Impliments:	nothing
*/
class cVertex
{
public:
	//ctor
	cVertex();
	//dtor
	~cVertex();
	//data members
	
	// the location of the vertex
	float x, y, z;

	// the colour of the vertex
	float r, g, b;

	// the normal of the vertex
	float nx, ny, nz;
};

#endif //!_cVertex_HG_
