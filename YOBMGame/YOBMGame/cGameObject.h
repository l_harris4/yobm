#ifndef _cGameObject_HG_
#define _cGameObject_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <string>
#include <Box2D\Box2D.h>

/*
*  Name:		cBullet
*  Summary:		Used as a super class for all other game objects
*  Impliments:	nothing
*/
class cGameObject
{
public:
	//ctor
	cGameObject();
	//dtor
	~cGameObject();

	/*
	*  Name:		Update
	*  Parameters:	double for the time
	*  Summary:		Updates the game object position based on time
	*  Returns:		nothing
	*/
	virtual void Update(double deltaTime);
	/*
	*  Name:		PerformAction
	*  Parameters:	nothing
	*  Summary:		a general method used to make an object perform 
	*				its 'function'
	*  Returns:		nothing
	*/
	virtual void PerformAction();
	/*
	*  Name:		Contact
	*  Parameters:	a cGameObject pointer of the game object this object
	*				has made contact with
	*  Summary:		a general method that is called when this object
	*				makes contact with another object
	*  Returns:		nothing
	*/
	virtual void Contact(cGameObject* otherObject);

	/*
	*  Name:		OrientAngle
	*  Parameters:	a vec2 representing the velocity
	*  Summary:		Orients the game object based on velocity
	*  Returns:		returns an orientation angle
	*/
	float cGameObject::OrientAngle(glm::vec2 velocity);

	//data members

	// the position of the game object
	glm::vec3 position;

	// the position of the game object last frame
	glm::vec3 oldPosition;

	// the pre rotation for the object
	glm::vec3 orientation;

	// the post rotation for the object
	glm::vec3 orientation2;

	// the scale that will be applied to the model
	float scale;

	// the velocity of the game object
	glm::vec3 vel;

	// the acceleration of the game object
	glm::vec3 accel;

	// whether or not the object will be updated in the physics step
	bool isUpdatedInPhysics;

	// whether or not the object will be affected by gravity
	bool affectedByGravity;

	// whether or not the object collided with another object
	bool collidedThisFrame;

	// whether or not the object could have been moved off its origin
	bool editable;

	// the world matrix of the object
	glm::mat4x4 mModel;

	// the velocity of the object last frame
	glm::vec3 holderVel;

	// the type of the object
	std::string typeOfObject;

	// the radius of the object
	float radius;

	// whether or not the object will be drawn wireframe
	bool isWireFrame;

	// the colour of the object
	glm::vec4 diffuseColour;

	// the original colour of the object (at the first frame)
	glm::vec4 originalColour;

	// the name of the mesh/model
	std::string meshName;

	// the physics body used with box2d to simulate collisions
	b2Body* physicsBody;
};

#endif // !_cGameObject_HG_
