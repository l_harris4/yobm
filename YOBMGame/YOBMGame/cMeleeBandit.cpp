#include "cMeleeBandit.h"
#include <vector>
#include <glm\trigonometric.hpp>
#include "cLuaBrain.h"
#include "cSoundEngine.h"
#include <sstream>

//the container of all sound objects
extern std::vector< cSoundEngine* > g_vecSoundObjects;
//Lua
extern cLuaBrain* p_LuaScripts;

//The vector of all the game objects
extern std::vector< cGameObject* >  g_vecGameObjects;

//ctor
cMeleeBandit::cMeleeBandit()
{
	//if the bandit is stunned or not
	this->stunned = false;
	//the stun time of the bandit
	this->stunTime = 50;
	//the longest time the bandit can be stunned
	this->stunTimeReset = this->stunTime;
}

//dtor
cMeleeBandit::~cMeleeBandit()
{
}

/*
*  Name:		Contact
*  Parameters:	a cGameObject pointer of the game object this object
*				has made contact with
*  Summary:		a method that is called when this object
*				makes contact with another object
*  Returns:		nothing
*/
void cMeleeBandit::Contact(cGameObject * otherObject)
{
	stunned = true;
	this->originalColour = this->diffuseColour;
	this->diffuseColour = glm::vec4(1, 0, 0, 1);
}

/*
*  Name:		Update()
*  Parameters:	double for the time
*  Summary:		Controls the bandits behavior
*  Returns:		nothing
*/
void cMeleeBandit::Update(double deltaTime)
{
	//The position of the bandit
	b2Vec2 physicsPosition;
	//if the objects has a physics body
	if (physicsBody != 0)
	{
		//if the bandit is stunned
		if (stunned)
		{
			physicsBody->SetLinearVelocity(b2Vec2(0, 0));
			stunTime -= 1;
			//if the bandit has gone through the stunned time
			if (stunTime < 0)
			{
				this->diffuseColour = this->originalColour;
				stunned = false;
				stunTime = stunTimeReset;

				//Lua Script Callback
				::p_LuaScripts->SetObjectVector(&(::g_vecSoundObjects));

				std::stringstream ssGotHit;

				std::string soundName = "grunt";
				ssGotHit << "MeleeHitByYOBM('";
				ssGotHit << soundName;
				ssGotHit << "') \n";

				std::cout << ssGotHit.str() << std::endl;

				::p_LuaScripts->LoadScript("MeleeHit", ssGotHit.str());

				//Update Lua Script
				::p_LuaScripts->Update();
			}// end if
		}// end if
		else
		{
			//the direction the bandit is facing
			b2Vec2 directionVector = b2Vec2(g_vecGameObjects[0]->position.x - position.x,
				g_vecGameObjects[0]->position.z - position.z);
			directionVector.Normalize();

			//the value to turn the bandit by
			float modificationValue = 5;
			directionVector = b2Vec2(directionVector.x* modificationValue, directionVector.y * modificationValue);

			//set the new velocity for the bandit
			physicsBody->SetLinearVelocity(directionVector);


			physicsPosition = physicsBody->GetPosition();
			position.x = physicsPosition.x;
			position.z = physicsPosition.y;


			b2Vec2 velocity = physicsBody->GetLinearVelocity();
			//if the player is moving
			if (velocity.x != 0 || velocity.y != 0)
			{
				//set the orientation for the bandit
				float angle = OrientAngle(glm::vec2(velocity.x, velocity.y));
				orientation2.y = glm::radians(glm::degrees(angle) - 90.0f);
			}// end if
		}// end else
	}// end if


}
