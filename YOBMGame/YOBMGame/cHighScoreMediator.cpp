#include "cHighScoreMediator.h"

//global variable used for the SetHighScore function
int rowsReturned = 0;

//ctor
cHighScoreMediator::cHighScoreMediator()
{
	OpenSQLiteDB(DB_FILENAME);
}

//dtor
cHighScoreMediator::~cHighScoreMediator()
{
	CloseSQLiteDB();
}

/*
*  Name:		OpenSQLiteDB
*  Parameters:	a string representing the name of the db
*  Summary:		creates the database instance
*  Returns:		a bool saying if this succeeded or not
*/
bool cHighScoreMediator::OpenSQLiteDB(const std::string& filename)
{
	int result = sqlite3_open(filename.c_str(), &db);
	if (CheckSQLiteErrors(result) == 1)
	{
		printf("Failed to open the sqlite database!\n");
		return false;
	}

	printf("Successfully opened the sqlite database!\n");
	return true;
}

/*
*  Name:		CheckSQLiteErrors
*  Parameters:	an int representing a possible error code from sqllite
*  Summary:		checks if an error has occurred
*  Returns:		an int saying if it succeeded or not
*/
int cHighScoreMediator::CheckSQLiteErrors(int result)
{
	if (result)
	{
		printf("SQLite error: %s\n", sqlite3_errmsg(db));
		return 1;
	}

	return 0;
}


/*
*  Name:		CloseSQLiteDB
*  Parameters:	nothing
*  Summary:		closes the connection to the sqllite database
*  Returns:		nothing
*/
void cHighScoreMediator::CloseSQLiteDB()
{
	printf("Closing SQLite db...\n");
	sqlite3_close(db);
}


static int SQLiteCallback(void *v, int argc, char** argv, char** azColName)
{
	rowsReturned = 0;
	for (int i = 0; i < argc; i++)
	{
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		rowsReturned++;
	}
	printf("\n");
	return 0;
}

/*
*  Name:		ExecuteSQLite
*  Parameters:	a char* that represents the sql to be run
*  Summary:		executes sql on the local database
*  Returns:		nothing
*/
void cHighScoreMediator::ExecuteSQLite(char * sql)
{
	char* errMsg = 0;
	int result = sqlite3_exec(db, sql, SQLiteCallback, 0, &errMsg);
	CheckSQLiteErrors(result);
	if (result != 0)
	{
		printf(errMsg);
		printf("\n");
		return;
	}
}



/*
*  Name:		SetUpTable
*  Parameters:	nothing
*  Summary:		creates the table in the database
*  Returns:		nothing
*/
void cHighScoreMediator::SetUpTable()
{
	char* errMsg = 0;
	char sql[MAX_SQL_LENGTH];
	sprintf_s(sql, "DROP TABLE player");
	ExecuteSQLite(sql);
	sprintf_s(sql, "CREATE TABLE player ( id int NOT NULL PRIMARY KEY, high_score int NOT NULL);");
	ExecuteSQLite(sql);
}

/*
*  Name:		SetHighScore
*  Parameters:	an int representing the id of the row to be updated, and an int respresenting the new high score
*  Summary:		sets a highscore in the db
*  Returns:		nothing
*/
void cHighScoreMediator::SetHighScore(int id, int score)
{

	char* errMsg = 0;
	char sql[MAX_SQL_LENGTH];
	sprintf_s(sql, "SELECT * FROM player WHERE id = %d;", id);
	ExecuteSQLite(sql);
	if (rowsReturned > 0)
	{
		//update the row
		char sql2[MAX_SQL_LENGTH];
		sprintf_s(sql2, "UPDATE player SET high_score = %d WHERE id = %d;", score, id);
		ExecuteSQLite(sql2);
		printf("Updated id(%d) to score(%d)\n", id, score);
	}
	else
	{
		//insert the row
		char* errMsg = 0;
		char sql[MAX_SQL_LENGTH];
		sprintf_s(sql, "INSERT INTO player (id, high_score) VALUES (%d, %d);", id, score);
		ExecuteSQLite(sql);
		printf("Inserted id(%d) score(%d) into the db\n", id, score);
	}
}

/*
*  Name:		GetHighScore
*  Parameters:	an int representing the id of the row/user we will to find the high score of
*  Summary:		prints out the high score of a single row
*  Returns:		nothing
*/
void cHighScoreMediator::GetHighScore(int id)
{
	char* errMsg = 0;
	char sql[MAX_SQL_LENGTH];
	sprintf_s(sql, "SELECT * FROM player WHERE id = %d;", id);
	ExecuteSQLite(sql);
}
