#ifndef _ModelUtilities_HG_
#define _ModelUtilities_HG_

#include <fstream>
#include <string>
#include "cMesh.h"

/*
*  Name:		ReadFileToToken()
*  Parameters:	an input file stream, and a string token
*  Summary:		Reads through a file until it reaches a certain string
*  Returns:		nothing
*/
void ReadFileToToken(std::ifstream &file, std::string token);

/*
*  Name:		LoadPlyFileIntoMesh()
*  Parameters:	a string representing a file name, and a mesh to load data into
*  Summary:		loads mesh data into a mesh object
*  Returns:		a bool saying if it worked or not
*/
bool LoadPlyFileIntoMesh(std::string filename, cMesh &theMesh);

/*
*  Name:		LoadPlyFileIntoMeshWithNormals()
*  Parameters:	a string representing a file name, and a mesh to load data into
*  Summary:		loads mesh data into a mesh object, this file however should already have normals
*  Returns:		a bool saying if it worked or not
*/
bool LoadPlyFileIntoMeshWithNormals(std::string filename, cMesh &theMesh);

#endif //!_ModelUtilities_HG_
