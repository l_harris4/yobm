#include "cGameObjectFactory.h"
#include "cGameObject.h"
#include "cPlayer.h"
#include "cMeleeBandit.h"
#include "cRangedBandit.h"
#include "cBullet.h"
#include <glm/glm.hpp>
#include <vector>

//the collection of all game objects
extern std::vector< cGameObject* >  g_vecGameObjects;

//setting the singleton instance to null
cGameObjectFactory* cGameObjectFactory::instance = nullptr;

/*
*  Name:		CreateGameObject()
*  Parameters:	a string representing the object type
*  Summary:		Creates a new game object of a certain type
*  Returns:		cGameObject*
*/
cGameObject * cGameObjectFactory::CreateGameObject(std::string objectType)
{
	//a pointer to the object that will be created
	cGameObject* pTheObject = NULL;

	//if type is player
	if (objectType == "player")
	{
		pTheObject = new cPlayer();
		pTheObject->typeOfObject = "player";
	}// end if
	//if type is ranged bandit
	else if (objectType == "banditR")
	{
		pTheObject = new cRangedBandit();
		pTheObject->typeOfObject = "banditR";
	}// end if
	//if type is melee bandit
	else if (objectType == "banditM")
	{
		pTheObject = new cMeleeBandit();
		pTheObject->typeOfObject = "banditM";
	}// end if
	else if (objectType == "bullet")
	{
		pTheObject = new cBullet();
		pTheObject->typeOfObject = "bullet";
	}// end if
	else
	{
		pTheObject = new cGameObject();
		pTheObject->typeOfObject = "object";
	}// end else

	return pTheObject;
}

/*
*  Name:		GetInstance()
*  Parameters:	nothing
*  Summary:		gets the actual instance of the factory
*  Returns:		cGameObjectFactory*
*/
cGameObjectFactory* cGameObjectFactory::GetInstance()
{
	//if we don't have an instance already
	if (instance == nullptr)
	{
		instance = new cGameObjectFactory();
	}// end if
	return instance;
}

