
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"
#include <glm/glm.hpp>
#include "cLightManager.h"
#include "cGameObjectFactory.h"
#include <fstream>

extern std::vector< cGameObject* >  g_vecGameObjects;
extern cGameObject* g_pTheDebugSphere;
extern cLightManager*		g_pLightManager;

/*
*  Name:		LoadModelsLightsFromFile()
*  Parameters:	nothing
*  Summary:		loads the meshes and lights from a file
*  Returns:		a bool saying if it worked or not
*/
bool LoadModelsLightsFromFile()
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	int lightIndex = -1;

	try {
		//while there are lines in the file to read
		while (getline(inFile, line))
		{
			//if the line is blank
			if (line == "")
			{
				continue;
			}// end if

			//if the line contains Object Start
			if (line == "Object Start") 
			{
				continue;
			}// end if

			//if the line contains Light Start
			if (line == "Light Start") 
			{
				::g_pLightManager->CreateLights(1, true);
				lightIndex++;
				continue;
			}// end if

			//if the line contains type
			if (line.find("type:") != std::string::npos) 
			{
				line.replace(0, 6, "");
				::g_vecGameObjects.push_back(cGameObjectFactory::GetInstance()->CreateGameObject(line));
				::g_vecGameObjects.back()->isUpdatedInPhysics = false;
				continue;
			}// end if

			//if the line contains gravity
			if (line.find("gravity:") != std::string::npos) 
			{
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->affectedByGravity = line == "true";
				continue;
			}// end if

			//if the line contains editable
			if (line.find("editable:") != std::string::npos) 
			{
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->editable = line == "true";
				continue;
			}// end if

			//if the line contains filename
			if (line.find("filename:") != std::string::npos) 
			{
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->meshName = line;
				continue;
			}// end if

			//if the line contains physics
			if (line.find("physics:") != std::string::npos) 
			{
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->isUpdatedInPhysics = line == "true";
				continue;
			}// end if

			//if the line contains wireframe
			if (line.find("wireframe:") != std::string::npos) 
			{
				line.replace(0, 11, "");
				::g_vecGameObjects.back()->isWireFrame = line == "true";
				continue;
			}// end if

			//if the line contains velocity
			if (line.find("velocity:") != std::string::npos) 
			{
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the x value is still required
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->vel.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_vecGameObjects.back()->vel.y = stof(number);
						number = "";
					}// end else
				}
				::g_vecGameObjects.back()->vel.z = stof(number);
				continue;
			}

			//if the line contains position
			if (line.find("position: ") != std::string::npos) 
			{
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the x value is still required
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->position.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_vecGameObjects.back()->position.y = stof(number);
						number = "";
					}// end else
				}
				::g_vecGameObjects.back()->position.z = stof(number);
				continue;
			}

			//if the line contains orientation
			if (line.find("orientation: ") != std::string::npos) 
			{
				line.replace(0, 13, "");

				std::string number;
				bool xValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the x value is still required
					else if (xValue)
					{
						xValue = false;
						::g_vecGameObjects.back()->orientation2.x = glm::radians(stof(number));
						number = "";
					}// end if
					else
					{
						::g_vecGameObjects.back()->orientation2.y = glm::radians(stof(number));
						number = "";
					}// end else
				}
				::g_vecGameObjects.back()->orientation2.z = glm::radians(stof(number));
				continue;
			}
			//if the line contains scale
			if (line.find("scale:") != std::string::npos) 
			{
				line.replace(0, 7, "");
				::g_vecGameObjects.back()->scale = stof(line);
				continue;
			}
			//if the line contains colour
			if (line.find("colour:") != std::string::npos) 
			{
				line.replace(0, 8, "");

				std::string number;
				bool rValue = true;
				//loop through all characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the red value is still required
					else if (rValue)
					{
						rValue = false;
						::g_vecGameObjects.back()->diffuseColour.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_vecGameObjects.back()->diffuseColour.y = stof(number);
						number = "";
					}// end else
				}
				::g_vecGameObjects.back()->diffuseColour.z = stof(number);
				::g_vecGameObjects.back()->diffuseColour.a = 1.0f;
				::g_vecGameObjects.back()->originalColour = ::g_vecGameObjects.back()->diffuseColour;
				continue;
			}

			//if the line contains a light filename
			if (line.find("filename_l:") != std::string::npos) 
			{
				line.replace(0, 12, "");
				::g_pLightManager->vecLights[lightIndex].fileName = line;
				cGameObject* pTempGO = new cGameObject();
				pTempGO->position = ::g_pLightManager->vecLights[lightIndex].position;
				::g_pLightManager->vecLights[lightIndex].gameObjectIndex = ::g_vecGameObjects.size();
				pTempGO->diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				pTempGO->meshName = line;
				pTempGO->isUpdatedInPhysics = false;
				pTempGO->scale = 1;
				pTempGO->radius = 1.0f;
				::g_vecGameObjects.push_back(pTempGO);		// Fastest way to add
				continue;
			}
			
			//if the line contains attentuation of a light
			if (line.find("attentuation:") != std::string::npos) 
			{
				line.replace(0, 14, "");

				std::string number;
				bool xValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the x value is still required
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].attenuation.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_pLightManager->vecLights[lightIndex].attenuation.y = stof(number);
						number = "";
					}// end else
				}
				::g_pLightManager->vecLights[lightIndex].attenuation.z = stof(number);
				continue;
			}

			//if the line contains the position of a light
			if (line.find("position_l:") != std::string::npos) 
			{
				line.replace(0, 12, "");

				std::string number;
				bool xValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if the character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the x value is still required
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].position.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_pLightManager->vecLights[lightIndex].position.y = stof(number);
						number = "";
					}// end else
				}
				::g_pLightManager->vecLights[lightIndex].position.z = stof(number);
				continue;
			}

			//if the colour of the light is on this line
			if (line.find("colour_l:") != std::string::npos) 
			{
				line.replace(0, 10, "");

				std::string number;
				bool rValue = true;
				//loop through the characters of the string
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					//if character is a comma
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}// end if
					//if the red value is still required
					else if (rValue)
					{
						rValue = false;
						::g_pLightManager->vecLights[lightIndex].diffuse.x = stof(number);
						number = "";
					}// end if
					else
					{
						::g_pLightManager->vecLights[lightIndex].diffuse.y = stof(number);
						number = "";
					}// end else
				}
				::g_pLightManager->vecLights[lightIndex].diffuse.z = stof(number);
				continue;
			}
		}

	}
	catch (std::exception ex)
	{
		return false;
	}
	return true;
}
