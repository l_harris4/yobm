#ifndef _cShaderManager_HG_
#define _cShaderManager_HG_

#include <string>
#include <vector>
#include <map>


/*
*  Name:		cShaderManager
*  Summary:		Manages the shaders
*  Impliments:	nothing
*/
class cShaderManager
{
public:

	/*
	*  Name:		cShader
	*  Summary:		Represents a shader
	*  Impliments:	nothing
	*/
	class cShader {
	public:
		// ctor
		cShader();
		// dtor
		~cShader();

		// data members
		enum eShaderType
		{
			VERTEX_SHADER,
			FRAGMENT_SHADER,
			UNKNOWN
		};

		// the shader type
		eShaderType shaderType;

		// shader id for uniform binding
		unsigned int ID;

		// vector of strings from each line in the shader source file
		std::vector<std::string> vecSource;

		// determines if the vector is more than one line
		bool bSourceIsMultiLine;

		// the shader path/filename
		std::string fileName;

		/*
		*  Name:		GetShaderTypeString()
		*  Parameters:	nothing
		*  Summary:		Retrieves the type of the shader
		*  Returns:		a string representing the type of the shader
		*/
		std::string GetShaderTypeString(void);
	};

	/*
	*  Name:		cShaderProgram
	*  Summary:		Represents a shader program
	*  Impliments:	nothing
	*/
	class cShaderProgram {
	public:
		// ctor
		cShaderProgram() : ID(0) {};
		// dtor
		~cShaderProgram() {};

		// data members

		// shader program id for uniform location binding 
		unsigned int ID;

		// filename without extention and path
		std::string friendlyName;
	};

	//ctor
	cShaderManager();
	//dtor
	~cShaderManager();

	/*
	*  Name:		UseShaderProgram()
	*  Parameters:	an int being the name of the shader
	*  Summary:		Tells the shader manager what is the current shader
	*  Returns:		a bool saying if this was successfully done or not
	*/
	bool UseShaderProgram(unsigned int ID);

	/*
	*  Name:		UseShaderProgram()
	*  Parameters:	a string being the name of the shader
	*  Summary:		Tells the shader manager what is the current shader
	*  Returns:		a bool saying if this was successfully done or not
	*/
	bool UseShaderProgram(std::string friendlyName);

	/*
	*  Name:		CreateProgramFromFile()
	*  Parameters:	a string being the name of the shader, a shader object for the vertext shader, and a shader object for the fragment shader
	*  Summary:		creates a shader program object
	*  Returns:		a bool saying if this was successfully done or not
	*/
	bool CreateProgramFromFile(std::string friendlyName, cShader &vertexShad, cShader &fragShader);

	/*
	*  Name:		SetBasePath()
	*  Parameters:	a string representing the new base path
	*  Summary:		sets where the files are stored
	*  Returns:		nothing
	*/
	void SetBasePath(std::string basepath);

	/*
	*  Name:		GetIDFromFriendlyName()
	*  Parameters:	a string representing the friendly name of the shader
	*  Summary:		gets the id of a shader from the friendly name
	*  Returns:		the id of the shader
	*/
	unsigned int GetIDFromFriendlyName(std::string friendlyName);

	/*
	*  Name:		GetLastError()
	*  Parameters:	nothing
	*  Summary:		gets the last error that occured
	*  Returns:		a string representing the error
	*/
	std::string GetLastError(void);


private:
	/*
	*  Name:		LoadSourceFromFile
	*  Parameters:	a cShader object
	*  Summary:		loads the appropriate shader info in from a file to the cShader object
	*  Returns:		a bool saying if this was successful or not
	*/
	bool LoadSourceFromFile(cShader &shader);

	/*
	*  Name:		CompileShaderFromSource
	*  Parameters:	a cShader object, and a string where we would store an error
	*  Summary:		attempts to compile a shader
	*  Returns:		a bool saying if this was successful or not
	*/
	bool CompileShaderFromSource(cShader &shader, std::string &error);

	/*
	*  Name:		WasThereACompileError
	*  Parameters:	an int representing a shader id, and a string where we would store
	*  Summary:		determines if a compile error happened or not
	*  Returns:		a bool saying if there was a compile error or not
	*/
	bool WasThereACompileError(unsigned int shaderID, std::string &errorText);

	/*
	*  Name:		WasThereALinkError
	*  Parameters:	an int representing a shader id, and a string where we would store
	*  Summary:		determines if a link error happened or not
	*  Returns:		a bool saying if there was a link error or not
	*/
	bool WasThereALinkError(unsigned int progID, std::string &errorText);

	// data members
	// string containing most recent error
	std::string lastError;

	// string containing the path without the filename 
	std::string basePath;

	// map linking the id with the shader program
	std::map< unsigned int, cShaderProgram > IDToShader;

	// map for interpretting string name as an int id
	std::map< std::string, unsigned int > nameToID;
};

#endif //!_cShaderManager_HG_