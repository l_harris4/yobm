#ifndef _JsonUtilities_HG_
#define _JsonUtilities_HG_

#include <string>
#include <vector>
#include "glm/glm.hpp"

// Forward Declarations
class cVAOMeshManager; 
class cLight;

/*
*  Name:		VectorToVec3()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec3
*  Returns:		glm::vec3
*/
glm::vec3 VectorToVec3(std::vector<float> vec);

/*
*  Name:		VectorToVec3Radians()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec3 with radian values
*  Returns:		glm::vec3
*/
glm::vec3 VectorToVec3Radians(std::vector<float> vec);

/*
*  Name:		VectorToVec4()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec4
*  Returns:		glm::vec4
*/
glm::vec4 VectorToVec4(std::vector<float> vec);

/*
*  Name:		LoadCameraFromJSON()
*  Parameters:	string containing file to load, glm::vec3 reference to camera positition, glm::vec3 reference to camera target
*  Summary:		Retrieve camera configurations from json file and apply them to referenced camera
*  Returns:		void
*/
void LoadCameraFromJSON(std::string jsonFile, glm::vec3 &camera, glm::vec3 &cameraTarget);

/*
*  Name:		LoadModelFilesFromJSON()
*  Parameters:	string containing file to load, int representing the shader id, object pointer to the VAO Manager, string for writting errors
*  Summary:		Retrieves the model files from json file to be loaded into the GPU
*  Returns:		void
*/
bool LoadModelFilesFromJSON(std::string jsonFile, int shaderID, cVAOMeshManager* pVAOManager, std::string &error);

/*
*  Name:		LoadGameObjectsFromJSON()
*  Parameters:	string containing file to load
*  Summary:		Reads the json file and creates the GameObjects in the scene and pushes them to the extern vector of GameObjects
*  Returns:		void
*/
void LoadGameObjectsFromJSON(std::string jsonFile);

/*
*  Name:		GetNumLightsFromJSON()
*  Parameters:	string containing file to load, reference to int passed to LightManager that creates the lights
*  Summary:		Reads the json file and counts the number of lights defined in the file
*  Returns:		void
*/
void GetNumLightsFromJSON(std::string jsonFile, int &numLights);

/*
*  Name:		LoadLightsFromJSON()
*  Parameters:	string containing file to load, reference to the vector that stores the Light Objects
*  Summary:		Reads the json file and modifies the properties of the created lights in the vector of lights
*  Returns:		void
*/
void LoadLightsFromJSON(std::string jsonFile, std::vector<cLight> &vec);

#endif