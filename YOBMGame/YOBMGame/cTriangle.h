#ifndef _cTriangle_HG_
#define _cTriangle_HG_

#include <glm/vec3.hpp>

/*
*  Name:		cTriangle
*  Summary:		Represents a graphical triangle that will be drawn to screen
*  Impliments:	nothing
*/
class cTriangle
{
public:
	// ctor
	cTriangle();
	// dtor
	~cTriangle();

	//data members
	// the id of the first vertex
	int vertex0ID;

	// the id of the second vertex
	int vertex1ID;

	// the id of the third vertex
	int vertex2ID;
};

/*
*  Name:		cPhysTriangle
*  Summary:		Represents triangle that will be used for physics calculations
*  Impliments:	nothing
*/
class cPhysTriangle
{
public:
	// the vertices of the triangle
	glm::vec3 vertex[3];
};

#endif
