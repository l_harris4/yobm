#include "cLightManager.h"
#include <vector>
#include <sstream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

//ctor
cLight::cLight()
{
	//the position of the light
	this->position = glm::vec3(0.0f);
	//the diffuse colour of the light
	this->diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	//the ambient colour of the light
	this->ambient = glm::vec3(0.2f, 0.2f, 0.2f);
	//the filename name of an object connected to the light
	this->fileName = "";
	//the index of the object connected to the light
	this->gameObjectIndex = -1;
	//the specular light of the light
	this->specular = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	//the attentuations of the light
	this->attenuation = glm::vec3(0.0f);
	this->attenuation.x = 0.0f;
	this->attenuation.y = 1.0f;
	this->attenuation.z = 0.0f;
	//the direction of the light
	this->direction = glm::vec3(0.0f);
	//the type params of the light
	this->typeParams = glm::vec4(0.0f);
	this->typeParams.y = 1000000.0f;
	//the various shader locations
	this->shaderLocPosition = -1;
	this->shaderLocDiffuse = -1;
	this->shaderLocAmbient = -1;
	this->shaderLocSpecular = -1;
	this->shaderLocAttenuation = -1;
	this->shaderLocDirection = -1;
	this->shaderLocTypeParams = -1;

	return;
}

//ctor
cLightManager::cLightManager()
{
	return;
}

//dtor
cLightManager::~cLightManager()
{
	return;
}

/*
*  Name:		CreateLights()
*  Parameters:	an int representing the number of lights in the scene, and a bool representing if we should keep the old values of
*				the lights while creating new ones
*  Summary:		Add a new light to the scene
*  Returns:		nothing
*/
void cLightManager::CreateLights(int numberOfLights, bool bKeepOldValues)
{
	//the number of lights in the vector already
	int newVectorSize = numberOfLights;
	newVectorSize = abs(newVectorSize) + this->vecLights.size();
	// Resize the vector
	this->vecLights.resize(newVectorSize, cLight());

	// if we don't want the original values
	if (!bKeepOldValues)
	{
		cLight tempLight;
		//go through all the lights
		for (int index = 0; index != this->vecLights.size(); index++)
		{
			this->vecLights[index] = tempLight;
		}// end for
	}// end if


	return;
}

/*
*  Name:		GenUniName()
*  Parameters:	an int representing the index of the light, and a string that we will use as part of the
*				name
*  Summary:		creates a unique name for a light
*  Returns:		a string being the unique name of light aspect
*/
std::string GenUniName(int lightIndex, std::string paramName)
{
	std::stringstream ssUniName;
	ssUniName << "myLight[" << lightIndex << "]." << paramName;
	return ssUniName.str();
}


/*
*  Name:		LoadShaderUniformLocations()
*  Parameters:	an int being the id of the shader we want to use
*  Summary:		connect the locations of light variables in the shader
*  Returns:		nothing
*/
void cLightManager::LoadShaderUniformLocations(int shaderID)
{
	//go through all the lights
	for (int index = 0; index != this->vecLights.size(); index++)
	{
		this->vecLights[index].shaderLocPosition
			= glGetUniformLocation(shaderID, GenUniName(index, "position").c_str());

		this->vecLights[index].shaderLocDiffuse = glGetUniformLocation(shaderID, GenUniName(index, "diffuse").c_str());
		this->vecLights[index].shaderLocAmbient = glGetUniformLocation(shaderID, GenUniName(index, "ambient").c_str());
		this->vecLights[index].shaderLocSpecular = glGetUniformLocation(shaderID, GenUniName(index, "specular").c_str());
		this->vecLights[index].shaderLocAttenuation = glGetUniformLocation(shaderID, GenUniName(index, "attenuation").c_str());
		this->vecLights[index].shaderLocDirection = glGetUniformLocation(shaderID, GenUniName(index, "direction").c_str());
		this->vecLights[index].shaderLocTypeParams = glGetUniformLocation(shaderID, GenUniName(index, "typeParams").c_str());
	}// end for
	return;
}

/*
*  Name:		CopyLightInformationToCurrentShader()
*  Parameters:	nothing
*  Summary:		Pushes the needed light information to the shader
*  Returns:		nothing
*/
void cLightManager::CopyLightInformationToCurrentShader(void)
{
	//go through all the lights
	for (int index = 0; index != this->vecLights.size(); index++)
	{
		cLight& pCurLight = this->vecLights[index];

		glUniform4f(pCurLight.shaderLocPosition,
			pCurLight.position.x,
			pCurLight.position.y,
			pCurLight.position.z,
			1.0f);

		glUniform4f(pCurLight.shaderLocDiffuse,
			pCurLight.diffuse.r,
			pCurLight.diffuse.g,
			pCurLight.diffuse.b,
			1.0f);

		glUniform4f(pCurLight.shaderLocAmbient,
			pCurLight.ambient.r,
			pCurLight.ambient.g,
			pCurLight.ambient.b,
			1.0f);

		glUniform4f(pCurLight.shaderLocSpecular,
			pCurLight.specular.r,
			pCurLight.specular.g,
			pCurLight.specular.b,
			pCurLight.specular.w);

		glUniform4f(pCurLight.shaderLocAttenuation,
			pCurLight.attenuation.x,
			pCurLight.attenuation.y,
			pCurLight.attenuation.z,
			1.0f);

		glUniform4f(pCurLight.shaderLocDirection,
			pCurLight.direction.x,
			pCurLight.direction.y,
			pCurLight.direction.z,
			1.0f);

		glUniform4f(pCurLight.shaderLocTypeParams,
			pCurLight.typeParams.x,
			pCurLight.typeParams.y,
			pCurLight.typeParams.z,
			pCurLight.typeParams.w);
	}// end for

	return;
}


const float cLight::DEFAULTINFINITEDISTANCE = 10000.0f;
const float cLight::DEFAULDIFFUSEACCURACYTHRESHOLD = 0.001f;

/*
*  Name:		CalcApproxDistFromAtten()
*  Parameters:	a float representing the wanted light level
*  Summary:		To get the distance from a light if we want a certain attentuation
*  Returns:		a float representing the distance away from the light
*/
float cLight::CalcApproxDistFromAtten(float targetLightLevel)
{
	return this->CalcApproxDistFromAtten(targetLightLevel, DEFAULDIFFUSEACCURACYTHRESHOLD);
}

/*
*  Name:		CalcApproxDistFromAtten()
*  Parameters:	a float representing the wanted light level, a float representing the desired accuracy of this function
*  Summary:		To get the distance from a light if we want a certain attentuation
*  Returns:		a float representing the distance away from the light
*/
float cLight::CalcApproxDistFromAtten(float targetLightLevel, float accuracy)	// Uses the defaults
{
	return this->CalcApproxDistFromAtten(targetLightLevel, accuracy, DEFAULTINFINITEDISTANCE, DEFAULTMAXITERATIONS);
}

/*
*  Name:		CalcApproxDistFromAtten()
*  Parameters:	a float representing the wanted light level, a float representing the desired accuracy of this function,
*				a float representing the infinite distance, and an unsigned int representing the max number of tries that this function will attempt to get the proper number
*  Summary:		To get the distance from a light if we want a certain attentuation
*  Returns:		a float representing the distance away from the light
*/
float cLight::CalcApproxDistFromAtten(float targetLightLevel, float accuracy,
	float infiniteDistance, unsigned int maxIterations)
{
	//if the target light level isn't 0
	if (targetLightLevel != 0.0f)
	{
		//if the accuracy is too high
		if ((accuracy * 10.0f) >= targetLightLevel * 10.0f)
		{	// Adjust the accuracy by a hundreth
			accuracy = targetLightLevel / 10.0f;
		}// end if
	}// end if

	float targetLightLevelLow = targetLightLevel - accuracy;
	float targetLightLevelHigh = targetLightLevel + accuracy;

	//if the light amount is greater tha the acceptable hight level
	if (this->CalcDiffuseFromAttenByDistance(DEFAULTINFINITEDISTANCE, accuracy) > targetLightLevelHigh)
	{
		return DEFAULTINFINITEDISTANCE;
	}// end if

	float distanceGuessLow = 0.0f;
	float distanceGuessHigh = DEFAULTINFINITEDISTANCE;

	//the iteration count
	unsigned int iterationCount = 0;

	while (iterationCount < maxIterations)
	{
		// Pick a distance between the high and low
		float curDistanceGuess = ((distanceGuessHigh - distanceGuessLow) / 2.0f) + distanceGuessLow;
		// 
		float curDiffuseAtGuessDistance = this->CalcDiffuseFromAttenByDistance(curDistanceGuess, DEFAULTZEROTHRESHOLD);
		// Could be three possibilities: too low, too high, or in between
		if (curDiffuseAtGuessDistance < targetLightLevelLow)
		{	// Light is too dark, so distance is to HIGH. Reduce and guess again.
			distanceGuessHigh = curDistanceGuess;		// Lower the high limit for the guesses
		}
		else if (curDiffuseAtGuessDistance > targetLightLevelHigh)
		{	// Light is too bright, so distance is to LOW. Increase and guess again
			distanceGuessLow = curDistanceGuess;
		}
		else
		{	// Nailed it! Light level is within range, so return this distance
			return curDistanceGuess;
		}

		iterationCount++;

	}// end while
	 // If we are here, then we ran out of iterations.
	 // Pick a distance between the low and high
	float distance = (distanceGuessHigh - distanceGuessLow) / 2.0f;

	return distance;
}

//static 
const float cLight::DEFAULTZEROTHRESHOLD = 0.0001f;

/*
*  Name:		CalcDiffuseFromAttenByDistance()
*  Parameters:	a float representing the distance from the light, a float representing where the light from the light is essentially zero
*  Summary:		Calculate the actual amount of light hitting a point
*  Returns:		float representing the amount of light from a certain distance away
*/
float cLight::CalcDiffuseFromAttenByDistance(float distance, float zeroThreshold)
{
	float diffuse = 1.0f;		// Assume full brightness

	float denominator = this->attenuation.x +
		this->attenuation.y * distance +
		this->attenuation.z * distance * distance;

	//if the denominator is less than the zero threshold
	if (denominator <= zeroThreshold)
	{	// Let's just say it's zero, shall we?
		diffuse = 1.0f;
	}// end if
	else
	{
		float atten = 1.0f / denominator;
		diffuse *= atten;
		if (diffuse > 1.0f)
		{
			diffuse = 1.0f;
		}
	}// end else
	return diffuse;
}
