#include "cPlayer.h"
#include <glm\glm.hpp>
#include <glm\trigonometric.hpp>
#include <glm\vec2.hpp>
#include <glm\vec3.hpp>
#include <math.h>
#include "cGameObjectFactory.h"
#include "cPhysicsManager.h"
#include "cBulletPool.h"
#include "cSoundEngine.h"
#include "cLuaBrain.h"
#include <sstream>

//the container of all sound objects
extern std::vector< cSoundEngine* > g_vecSoundObjects;
//the container of all the game objects
extern std::vector< cGameObject* >  g_vecGameObjects;
//the physics manager for the whole world
extern cPhysicsManager* g_pPhysicsManager;
cSoundEngine *tempSoundObj = 0;
//Lua
extern cLuaBrain* p_LuaScripts;
//ctor
cPlayer::cPlayer()
{
	//if the player is stunned or not
	this->stunned = false;
}

//dtor
cPlayer::~cPlayer()
{
}

/*
*  Name:		Fire()
*  Parameters:	nothing
*  Summary:		fires a bullet/missile
*  Returns:		nothing
*/
void cPlayer::Fire()
{
	b2Vec2 tempVel(cos(orientation2.y), sin(orientation2.y));
	tempVel.Normalize();
	tempVel.y = -tempVel.y;

	//Lua Script Callback
	::p_LuaScripts->SetObjectVector(&(::g_vecSoundObjects));

	std::stringstream ssFireBullet;

	std::string soundName = "bullet";
	ssFireBullet << "FireBullets('";
	ssFireBullet << soundName;
	ssFireBullet << "') \n";

	std::cout << ssFireBullet.str() << std::endl;

	::p_LuaScripts->LoadScript("FireBullet", ssFireBullet.str());

	//Update Lua script
	::p_LuaScripts->Update();


	
	//create a new game object and add it to the vector of objects
	// Create cGameObject using factory
	cGameObject* pTempGO = cBulletPool::GetInstance()->Acquire();

	//if the object doesn't have a physics body
	if (!pTempGO->physicsBody)
	{
		pTempGO->meshName = "sphere";
		pTempGO->scale = 0.3;
		pTempGO->isWireFrame = false;
		pTempGO->isUpdatedInPhysics = true;

		// Need to Convert these elements to glm::vec3 and vec4s
		pTempGO->position = glm::vec3(position.x + (tempVel.x * 5), position.y + 3, position.z + (tempVel.y * 5));
		pTempGO->diffuseColour = this->diffuseColour;

		//create a physics object to be linked to that object
		b2BodyDef sphereBodyDef;
		sphereBodyDef.position.Set(pTempGO->position.x, pTempGO->position.z);
		// This allows the object to move
		sphereBodyDef.type = b2_dynamicBody;						
		pTempGO->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);
		pTempGO->physicsBody->SetUserData(pTempGO);

		b2CircleShape circleShape;
		circleShape.m_p.Set(0.0f, 0.0f);
		circleShape.m_radius = pTempGO->scale;
		pTempGO->physicsBody->CreateFixture(&circleShape, 1.0f);

		int modificationValue = 20;
		tempVel = b2Vec2(tempVel.x * modificationValue, tempVel.y * modificationValue);
		pTempGO->physicsBody->SetLinearVelocity(tempVel);

		// Push the GameObjec to the global vector
		g_vecGameObjects.push_back(pTempGO);
	}// end if
	else
	{
		pTempGO->physicsBody->GetWorld()->DestroyBody(pTempGO->physicsBody);

		pTempGO->position = glm::vec3(position.x + (tempVel.x * 5), position.y + 3, position.z + (tempVel.y * 5));
		pTempGO->diffuseColour = this->diffuseColour;

		//create a physics object to be linked to that object

		b2BodyDef sphereBodyDef;
		sphereBodyDef.position.Set(pTempGO->position.x, pTempGO->position.z);
		sphereBodyDef.type = b2_dynamicBody;						// This allows the object to move
		pTempGO->physicsBody = ::g_pPhysicsManager->CreateBody(&sphereBodyDef);
		pTempGO->physicsBody->SetUserData(pTempGO);

		b2CircleShape circleShape;
		circleShape.m_p.Set(0.0f, 0.0f);							// See how the midpoint at 0, 0 works.
		circleShape.m_radius = pTempGO->scale;
		pTempGO->physicsBody->CreateFixture(&circleShape, 1.0f);

		int modificationValue = 20;
		tempVel = b2Vec2(tempVel.x * modificationValue, tempVel.y * modificationValue);
		pTempGO->physicsBody->SetLinearVelocity(tempVel);
	}
}


/*
*  Name:		Update()
*  Parameters:	double for the time
*  Summary:		updates the player character based on input
*  Returns:		nothing
*/
void cPlayer::Update(double deltaTime)
{
	//the position of the player
	b2Vec2 physicsPosition;
	//if the player has a physics body
	if (physicsBody != 0)
	{
		physicsPosition = physicsBody->GetPosition();
		position.x = physicsPosition.x;
		position.z = physicsPosition.y;
	}// end if

	b2Vec2 velocity = physicsBody->GetLinearVelocity();
	//if the player is moving
	if (velocity.x != 0 || velocity.y != 0)
	{
		float angle = OrientAngle(glm::vec2(velocity.x, velocity.y));
		orientation2.y = glm::radians(glm::degrees(angle) - 90.0f);
	}// end if
}

/*
*  Name:		PerformAction()
*  Parameters:	void
*  Summary:		Used by the Mediator, this triggers the Fire() method
*  Returns:		nothing
*/
void cPlayer::PerformAction()
{
	Fire();
}
