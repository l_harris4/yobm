#ifndef _cPlayerHG_
#define _cPlayerHG_
#include "cGameObject.h"

/*
*  Name:		cPlayer
*  Summary:		Represents Ye Olde Bazooka Man / the main player
*  Impliments:	cGameObject
*/
class cPlayer : public cGameObject
{
public:
	// ctor
	cPlayer();

	// dtor
	~cPlayer();

	/*
	*  Name:		Fire()
	*  Parameters:	nothing
	*  Summary:		fires a bullet/missile
	*  Returns:		nothing
	*/
	void Fire();

	// boolean storing the state if the player has been stunned or not
	bool stunned;

	/*
	*  Name:		Update()
	*  Parameters:	double for the time
	*  Summary:		updates the player character based on input
	*  Returns:		nothing
	*/
	virtual void Update(double deltaTime);

	/*
	*  Name:		PerformAction()
	*  Parameters:	void
	*  Summary:		Used by the Mediator, this triggers the Fire() method
	*  Returns:		nothing
	*/
	virtual void PerformAction();
};

#endif // !_cPlayerHG_
