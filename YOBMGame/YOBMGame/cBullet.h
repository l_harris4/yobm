#ifndef _cBullet_HG_
#define _cBullet_HG_
#include "cGameObject.h"

/*
*  Name:		cBullet
*  Summary:		Used as a projectile that bandits fire at the player
*  Impliments:	cGameObject
*/
class cBullet : public cGameObject
{
public:
	//ctor
	cBullet();
	//dtor
	~cBullet();
	/*
	*  Name:		Update
	*  Parameters:	double for the time
	*  Summary:		Updates the bullet position based on time
	*  Returns:		nothing
	*/
	virtual void Update(double deltaTime);

	/*
	*  Name:		Contact
	*  Parameters:	a pointer to a cGameObject
	*  Summary:		is a method that is called when the bullet hits something
	*  Returns:		nothing
	*/
	virtual void Contact(cGameObject* otherObject);

	//to keep track of if the bullet has been returned to the bullet pool
	bool inPool;
};

#endif // !_cBullet_HG_
