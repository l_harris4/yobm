#include "cMesh.h"

#include <glm/glm.hpp>		// cross product (I hope)

//ctor
cMesh::cMesh()
{
	//the number of vertices in the mesh
	this->numberOfVertices = 0;
	//the number of triangles in the mesh
	this->numberOfTriangles = 0;
	//the max extent of the mesh
	this->maxExtent = 0.0f;
	//the scale needed to fit the mesh into a 1 by 1 by 1 box
	this->scaleForUnitBBox = 1.0f;
	return;
}

//dtor
cMesh::~cMesh()
{
	return;
}

/*
*  Name:		CalculateExtents()
*  Parameters:	nothing
*  Summary:		calculate the extents of the mesh
*  Returns:		nothing
*/
void cMesh::CalculateExtents(void)
{
	// Assume 1st vertex is both max and min
	this->minXYZ.x = this->pVertices[0].x;
	this->minXYZ.y = this->pVertices[0].y;
	this->minXYZ.z = this->pVertices[0].z;
	this->maxXYZ.x = this->pVertices[0].x;
	this->maxXYZ.y = this->pVertices[0].y;
	this->maxXYZ.z = this->pVertices[0].z;

	//go through all the vertices
	for (int index = 0; index != this->numberOfVertices; index++)
	{
		if (this->pVertices[index].x < this->minXYZ.x)
		{
			this->minXYZ.x = this->pVertices[index].x;
		}// end if
		if (this->pVertices[index].x > this->maxXYZ.x)
		{
			this->maxXYZ.x = this->pVertices[index].x;
		}// end if
		if (this->pVertices[index].y < this->minXYZ.y)
		{
			this->minXYZ.y = this->pVertices[index].y;
		}// end if
		if (this->pVertices[index].y > this->maxXYZ.y)
		{
			this->maxXYZ.y = this->pVertices[index].y;
		}// end if
		if (this->pVertices[index].z < this->minXYZ.z)
		{
			this->minXYZ.z = this->pVertices[index].z;
		}// end if
		if (this->pVertices[index].z > this->maxXYZ.z)
		{
			this->maxXYZ.z = this->pVertices[index].z;
		}// end if

	}// end for

	this->maxExtentXYZ.x = this->maxXYZ.x - this->minXYZ.x;
	this->maxExtentXYZ.y = this->maxXYZ.y - this->minXYZ.y;
	this->maxExtentXYZ.z = this->maxXYZ.z - this->minXYZ.z;

	this->maxExtent = this->maxExtentXYZ.x;
	if (this->maxExtent < this->maxExtentXYZ.y)
	{	// Y is bigger
		this->maxExtent = this->maxExtentXYZ.y;
	}// end if
	if (this->maxExtent < this->maxExtentXYZ.z)
	{	// Z is bigger
		this->maxExtent = this->maxExtentXYZ.z;
	}// end if
	//
	this->scaleForUnitBBox = 1.0f / this->maxExtent;

	return;
}


/*
*  Name:		FlattenIndexedModel()
*  Parameters:	nothing
*  Summary:		Takes an indexed model and makes just a vertex array model
*  Returns:		nothing
*/
void cMesh::FlattenIndexedModel(void)
{
	int origNumVertices = this->numberOfVertices;
	cVertex* pVertOrig = new cVertex[this->numberOfVertices];

	//go through all the vertices
	for (int index = 0; index < origNumVertices; index++)
	{
		pVertOrig[index] = this->pVertices[index];
	}// end for

	delete[] this->pVertices;

	int numberOfVertsNeeded = this->numberOfTriangles * 3;
	numberOfVertsNeeded += 30;
	this->pVertices = new cVertex[numberOfVertsNeeded];

	int triIndex = 0;
	int vertIndex = 0;
	//go through the triangles
	for (; triIndex < this->numberOfTriangles;
		triIndex++, vertIndex += 3)
	{
		int triVert0_index = this->pTriangles[triIndex].vertex0ID;
		int triVert1_index = this->pTriangles[triIndex].vertex1ID;
		int triVert2_index = this->pTriangles[triIndex].vertex2ID;

		cVertex V0 = pVertOrig[triVert0_index];
		cVertex V1 = pVertOrig[triVert1_index];
		cVertex V2 = pVertOrig[triVert2_index];

		this->pVertices[vertIndex + 0] = V0;
		this->pVertices[vertIndex + 1] = V1;
		this->pVertices[vertIndex + 2] = V2;
	}// end for

	this->numberOfVertices = vertIndex - 3;
	return;
}

/*
*  Name:		CalculateNormals()
*  Parameters:	nothing
*  Summary:		calculate the normals of the mesh if none were provided
*  Returns:		nothing
*/
void cMesh::CalculateNormals(void)
{

	//go through all the vertices
	for (int vertIndex = 0; vertIndex != this->numberOfVertices; vertIndex++)
	{
		this->pVertices[vertIndex].nx = 0.0f;
		this->pVertices[vertIndex].ny = 0.0f;
		this->pVertices[vertIndex].nz = 0.0f;
	}// end for

	//go through all the triangles
	for (int triIndex = 0; triIndex != this->numberOfTriangles; triIndex++)
	{
		cTriangle curTri = this->pTriangles[triIndex];

		// Calculate normal for each vertex
		glm::vec3 vertA = glm::vec3(this->pVertices[curTri.vertex0ID].x,
			this->pVertices[curTri.vertex0ID].y,
			this->pVertices[curTri.vertex0ID].z);

		glm::vec3 vertB = glm::vec3(this->pVertices[curTri.vertex1ID].x,
			this->pVertices[curTri.vertex1ID].y,
			this->pVertices[curTri.vertex1ID].z);

		glm::vec3 vertC = glm::vec3(this->pVertices[curTri.vertex2ID].x,
			this->pVertices[curTri.vertex2ID].y,
			this->pVertices[curTri.vertex2ID].z);

		// Cross of A-B and A-C (normal at vertex A)
		glm::vec3 normVec0 = glm::normalize(glm::cross(vertB - vertA, vertC - vertA));

		// Cross of B-A and B-C (normal at vertex B)
		glm::vec3 normVec1 = glm::normalize(glm::cross(vertA - vertB, vertC - vertB));

		// Cross of C-A and C-B (normal at vertex C)
		glm::vec3 normVec2 = glm::normalize(glm::cross(vertA - vertC, vertB - vertC));

		// Add the values to the current normals (vert A)
		this->pVertices[curTri.vertex0ID].nx += normVec0.x;
		this->pVertices[curTri.vertex0ID].ny += normVec0.y;
		this->pVertices[curTri.vertex0ID].nz += normVec0.z;

		// Add the values to the current normals (vert B)
		this->pVertices[curTri.vertex1ID].nx += normVec1.x;
		this->pVertices[curTri.vertex1ID].ny += normVec1.y;
		this->pVertices[curTri.vertex1ID].nz += normVec1.z;

		// Add the values to the current normals (vert C)
		this->pVertices[curTri.vertex2ID].nx += normVec2.x;
		this->pVertices[curTri.vertex2ID].ny += normVec2.y;
		this->pVertices[curTri.vertex2ID].nz += normVec2.z;
	}// end for

	//go through the vertices
	for (int vertIndex = 0; vertIndex != this->numberOfVertices; vertIndex++)
	{
		glm::vec3 norm = glm::vec3(this->pVertices[vertIndex].nx,
			this->pVertices[vertIndex].ny,
			this->pVertices[vertIndex].nz);
		// It's value DIV length
		glm::normalize(norm);
		// divide the xyz by the length of the vector
		this->pVertices[vertIndex].nx = norm.x;
		this->pVertices[vertIndex].ny = norm.y;
		this->pVertices[vertIndex].nz = norm.z;
	}// end for
	return;
}


/*
*  Name:		GeneratePhysicsTriangles()
*  Parameters:	nothing
*  Summary:		Generates triangles with vertex location build in, instead of just ID's
*  Returns:		nothing
*/
void cMesh::GeneratePhysicsTriangles(void)
{
	//go through all the triangles
	for (int triIndex = 0; triIndex < this->numberOfTriangles; triIndex++)
	{
		int triVert0_index = this->pTriangles[triIndex].vertex0ID;
		int triVert1_index = this->pTriangles[triIndex].vertex1ID;
		int triVert2_index = this->pTriangles[triIndex].vertex2ID;

		cPhysTriangle tempTri;

		cVertex V0 = this->pVertices[triVert0_index];
		tempTri.vertex[0].x = V0.x;
		tempTri.vertex[0].y = V0.y;
		tempTri.vertex[0].z = V0.z;

		cVertex V1 = this->pVertices[triVert1_index];
		tempTri.vertex[1].x = V1.x;
		tempTri.vertex[1].y = V1.y;
		tempTri.vertex[1].z = V1.z;

		cVertex V2 = this->pVertices[triVert2_index];
		tempTri.vertex[2].x = V2.x;
		tempTri.vertex[2].y = V2.y;
		tempTri.vertex[2].z = V2.z;

	}// end for

	return;
}
