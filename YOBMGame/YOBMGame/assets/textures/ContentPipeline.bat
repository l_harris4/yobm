@echo off
title content pipeline

cd .\assets\textures\

echo Lossy compression of png files using pngquant

for %%f in (*.png) do (
	Echo.%%f | findstr /C:"_1">nul && (Echo.TRUE) || (	pngquant --speed 2 --quality=65-80 "%%~nf.png" --ext "_1.png")
)

for %%f in (*.png) do (
	Echo.%%f | findstr /C:"_1">nul && (Echo.TRUE) || ( del "%%~nf.png")
)

echo Lossless compression of png files using optipng

for %%f in (*_1*.*) do (
	Echo.%%f | findstr /C:"_2">nul && (Echo.TRUE) || ( optipng -o3 "%%~nf.png" -out "%%~nf_2.png")
)

for %%f in (*.png) do (
	Echo.%%f | findstr /C:"_2">nul && (Echo.TRUE) || ( del "%%~nf.png")
)


echo Conversion of png files into DDS

for %%f in (*_2*.*) do (
	Echo.%%f | findstr /C:"_3">nul && (Echo.TRUE) || ( 	PVRTexToolCLI -i "%%~nf.png" -o "%%~nf_3.dds" -f PVRTC1_4 -potcanvas -)
)

echo Task maybe completed...go check
pause
