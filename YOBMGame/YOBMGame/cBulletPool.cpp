#include "cBulletPool.h"
#include "cBullet.h"
#include "cGameObject.h"
#include "cGameObjectFactory.h"
#include <iostream>

//The instance of the bullet pool
cBulletPool* cBulletPool::mInstance;

//ctor
cBulletPool::cBulletPool() {
}

/*
*  Name:		GetInstance
*  Parameters:	nothing
*  Summary:		returns the instance of the singleton bullet pool
*  Returns:		a pointer to a cGameObject
*/
cBulletPool* cBulletPool::GetInstance() {
	//if the bullet pool doesn't yet exist
	if (cBulletPool::mInstance == 0) {
		cBulletPool::mInstance = new cBulletPool();
	}// end if
	return cBulletPool::mInstance;
}

//dtor
cBulletPool::~cBulletPool() {
	mPooledObjects.clear();
}

/*
*  Name:		Acquire
*  Parameters:	nothing
*  Summary:		returns a pointer to a bullet in the pool
*  Returns:		a pointer to a cGameObject
*/
cGameObject* cBulletPool::Acquire() 
{
	//if the pool is empty, then make a new bullet
	if (mPooledObjects.size() == 0) {
		cGameObject* poolObject = cGameObjectFactory::GetInstance()->CreateGameObject("bullet");
		return (cGameObject*)poolObject;
	}// end if

	//the bullet object that will be returned
	cGameObject* poolObject = mPooledObjects.back();
	((cBullet*)poolObject)->inPool = false;
	mPooledObjects.pop_back();
	return poolObject;
}

/*
*  Name:		Release
*  Parameters:	a cGameObject pointer of the bullet
*  Summary:		releases the bullet from the pool to be used in the game
*  Returns:		nothing
*/
void cBulletPool::Release(cGameObject* poolObject) {

	((cBullet*)poolObject)->inPool = true;
	mPooledObjects.push_back(poolObject);
}
