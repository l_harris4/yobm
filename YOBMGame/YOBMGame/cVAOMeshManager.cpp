#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "cVAOMeshManager.h"
#include "cVertex.h"
#include "cMesh.h"

//ctor
cVAOMeshManager::cVAOMeshManager()
{
	return;
}

//dtor
cVAOMeshManager::~cVAOMeshManager()
{
	return;
}

/*
*  Name:		LoadMeshIntoVAO()
*  Parameters:	a mesh object to be changed and an int representing the current shader
*  Summary:		loads a mesh into a VAO object
*  Returns:		a bool saying if it worked or not
*/
bool cVAOMeshManager::LoadMeshIntoVAO(cMesh &theMesh, int shaderID)
{
	sVAOInfo theVAOInfo;

	// Create a Vertex Array Object (VAO)
	glGenVertexArrays(1, &(theVAOInfo.VaoID));
	glBindVertexArray(theVAOInfo.VaoID);


	glGenBuffers(1, &(theVAOInfo.vertexBufferID));
	glBindBuffer(GL_ARRAY_BUFFER, theVAOInfo.vertexBufferID);

	// Allocate the global vertex array
	cVertex* pVertices = new cVertex[theMesh.numberOfVertices];

	//loop through all the meshes vertices
	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{
		pVertices[index].x = theMesh.pVertices[index].x;
		pVertices[index].y = theMesh.pVertices[index].y;
		pVertices[index].z = theMesh.pVertices[index].z;

		pVertices[index].r = theMesh.pVertices[index].r;
		pVertices[index].g = theMesh.pVertices[index].g;
		pVertices[index].b = theMesh.pVertices[index].b;

		pVertices[index].nx = theMesh.pVertices[index].nx;
		pVertices[index].ny = theMesh.pVertices[index].ny;
		pVertices[index].nz = theMesh.pVertices[index].nz;
	}// end for

	// Copy the local vertex array into the GPUs memory
	int sizeOfGlobalVertexArrayInBytes = sizeof(cVertex) * theMesh.numberOfVertices;
	glBufferData(GL_ARRAY_BUFFER,
		sizeOfGlobalVertexArrayInBytes,
		pVertices,
		GL_STATIC_DRAW);

	// Get rid of local vertex array
	delete[] pVertices;

	glGenBuffers(1, &(theVAOInfo.indexBufferID));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, theVAOInfo.indexBufferID);

	int numberOfIndices = theMesh.numberOfTriangles * 3;

	unsigned int* indexArray = new unsigned int[numberOfIndices];

	// Copy the triangle data into this 1D array 
	int triIndex = 0;
	int indexIndex = 0;

	//loop through the meshes triangles
	for (; triIndex < theMesh.numberOfTriangles; triIndex++, indexIndex += 3)
	{
		indexArray[indexIndex + 0] = theMesh.pTriangles[triIndex].vertex0ID;
		indexArray[indexIndex + 1] = theMesh.pTriangles[triIndex].vertex1ID;
		indexArray[indexIndex + 2] = theMesh.pTriangles[triIndex].vertex2ID;
	}// end for

	//loop through the meshes triangles
	for (int triIndex = 0; triIndex < theMesh.numberOfTriangles; triIndex++)
	{
		int triVert0_index = theMesh.pTriangles[triIndex].vertex0ID;
		int triVert1_index = theMesh.pTriangles[triIndex].vertex1ID;
		int triVert2_index = theMesh.pTriangles[triIndex].vertex2ID;
		cPhysTriangle tempTri;

		cVertex V0 = theMesh.pVertices[triVert0_index];
		tempTri.vertex[0].x = V0.x;
		tempTri.vertex[0].y = V0.y;
		tempTri.vertex[0].z = V0.z;

		cVertex V1 = theMesh.pVertices[triVert1_index];
		tempTri.vertex[1].x = V1.x;
		tempTri.vertex[1].y = V1.y;
		tempTri.vertex[1].z = V1.z;

		cVertex V2 = theMesh.pVertices[triVert2_index];
		tempTri.vertex[2].x = V2.x;
		tempTri.vertex[2].y = V2.y;
		tempTri.vertex[2].z = V2.z;

		theVAOInfo.vecPhysTris.push_back(tempTri);

	}// end for

	 // note number of indices is number of triangles x 3
	int sizeOfIndexArrayInBytes = sizeof(unsigned int) * numberOfIndices;

	glBufferData(GL_ELEMENT_ARRAY_BUFFER,		// "index buffer" or "vertex index buffer"
		sizeOfIndexArrayInBytes,
		indexArray,
		GL_STATIC_DRAW);

	// Don't need local array anymore (like the on in CPU RAM)
	delete[] indexArray;

	// At this point, the vertex and index buffers are stored on GPU

	// Now set up the vertex layout (for this shader)
	GLuint vpos_location = glGetAttribLocation(shaderID, "vPos");
	GLuint vcol_location = glGetAttribLocation(shaderID, "vCol");
	GLuint vnorm_location = glGetAttribLocation(shaderID, "vNorm");

	glEnableVertexAttribArray(vpos_location);
	const int offsetOf_x_into_cVertex = 0;
	glVertexAttribPointer(vpos_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,		
		(void*)offsetof(cVertex, x));


	glEnableVertexAttribArray(vcol_location);
	const int offsetOf_r_into_cVertex = 3;
	glVertexAttribPointer(vcol_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,
		(void*)offsetof(cVertex, r));

	// Now we also have normals on the vertex
	glEnableVertexAttribArray(vnorm_location);
	glVertexAttribPointer(vnorm_location,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 9,
		(void*)offsetof(cVertex, nx));

	// Copy the information into the VAOInfo structure
	theVAOInfo.numberOfIndices = theMesh.numberOfTriangles * 3;
	theVAOInfo.numberOfTriangles = theMesh.numberOfTriangles;
	theVAOInfo.numberOfVertices = theMesh.numberOfVertices;
	theVAOInfo.friendlyName = theMesh.name;
	theVAOInfo.shaderID = shaderID;


	theMesh.CalculateExtents();
	theVAOInfo.scaleForUnitBBox = theMesh.scaleForUnitBBox;

	// Store the VAO info by mesh name
	this->mapNameToVAO[theMesh.name] = theVAOInfo;
	// Unbind the VAO first
	glBindVertexArray(0);

	// Unbind (release) everything
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(vcol_location);
	glDisableVertexAttribArray(vpos_location);

	return true;
}

/*
*  Name:		LookupVAOFromName()
*  Parameters:	string being the name of the mesh, and the VAOInfo object that we want
*				the information to be contained int
*  Summary:		finds the relavent vao object to a certain mesh
*  Returns:		a bool sayig if it worked or not
*/
bool cVAOMeshManager::LookupVAOFromName(std::string name, sVAOInfo &theVAOInfo)
{
	// look up in map for the name of the mesh we want to draw

	std::map< std::string, sVAOInfo >::iterator itVAO = this->mapNameToVAO.find(name);

	//if nothing was found
	if (itVAO == this->mapNameToVAO.end())
	{
		return false;
	}

	theVAOInfo = itVAO->second;	
}
