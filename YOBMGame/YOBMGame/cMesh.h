#ifndef _cMesh_HG_
#define _cMesh_HG_

#include "cVertex.h"
#include "cTriangle.h" 

static const int MAX_VERTEX_ARRAY_SIZE = 4096;
static const int MAX_INDEX_ARRAY_SIZE = 4096;

#include <string>
#include <glm/vec3.hpp>
#include <vector>

/*
*  Name:		cMesh
*  Summary:		to represent the mesh that will be shown on screen, the graphical component of an object
*  Impliments:	nothing
*/
class cMesh
{
public:
	// ctor
	cMesh();
	// dtor
	~cMesh();

	// data members

	// the friendly name of the mesh
	std::string name;

	// a pointer to the vertices of the mesh
	cVertex* pVertices;

	// the total number of vertices
	int numberOfVertices;

	// a pointer to the triangles of the mesh
	cTriangle* pTriangles;

	// the total number of triangles
	int numberOfTriangles;

	// the minimum point of the mesh
	glm::vec3 minXYZ;

	// the maximum point of the mesh
	glm::vec3 maxXYZ;

	// the maximum extent of the mesh in a vec 3
	glm::vec3 maxExtentXYZ;

	// the max extent distance
	float maxExtent;

	// the ratio between 1 and this number
	float scaleForUnitBBox;

	/*
	*  Name:		GeneratePhysicsTriangles()
	*  Parameters:	nothing
	*  Summary:		Generates triangles with vertex location build in, instead of just ID's
	*  Returns:		nothing
	*/
	void GeneratePhysicsTriangles(void);

	/*
	*  Name:		FlattenIndexedModel()
	*  Parameters:	nothing
	*  Summary:		Takes an indexed model and makes just a vertex array model
	*  Returns:		nothing
	*/
	void FlattenIndexedModel(void);

	/*
	*  Name:		CalculateExtents()
	*  Parameters:	nothing
	*  Summary:		calculate the extents of the mesh
	*  Returns:		nothing
	*/
	void CalculateExtents(void);

	/*
	*  Name:		CalculateNormals()
	*  Parameters:	nothing
	*  Summary:		calculate the normals of the mesh if none were provided
	*  Returns:		nothing
	*/
	void CalculateNormals(void);
};
#endif // !_cGameObject_HG_
