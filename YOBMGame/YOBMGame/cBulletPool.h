#ifndef cBulletPool_HG
#define cBulletPool_HG

#include <map>
#include <vector>
#include <string>

//forward declarations
class cBullet;
class cGameObject;

using namespace std;

/*
*  Name:		cBulletPool
*  Summary:		Used to keep track of bullet objects
*  Impliments:	nothing
*/
class cBulletPool {
public:
	//dtor
	~cBulletPool();

	/*
	*  Name:		GetInstance
	*  Parameters:	nothing
	*  Summary:		returns the instance of the singleton bullet pool
	*  Returns:		a pointer to a cGameObject
	*/
	static cBulletPool* GetInstance();

	/*
	*  Name:		Acquire
	*  Parameters:	nothing
	*  Summary:		returns a pointer to a bullet in the pool
	*  Returns:		a pointer to a cGameObject
	*/
	cGameObject* Acquire();

	/*
	*  Name:		Release
	*  Parameters:	a cGameObject pointer of the bullet
	*  Summary:		releases the bullet from the pool to be used in the game
	*  Returns:		nothing
	*/
	void Release(cGameObject* bullet);

private:
	//ctor
	cBulletPool();

	//the instance of the bullet pool
	static cBulletPool* mInstance;

	//a vector containing all of the bullets
	vector<cGameObject*> mPooledObjects;
};

#endif //!cBulletPool_HG
