#include "cBullet.h"
#include <vector>
#include <set>
#include "cBulletPool.h"
#include "cLuaBrain.h"
#include "cSoundEngine.h"
#include <sstream>

//the container of all sound objects
extern std::vector< cSoundEngine* > g_vecSoundObjects;
//Lua
extern cLuaBrain* p_LuaScripts;
//The collection of all the game objects
extern std::vector< cGameObject* >  g_vecGameObjects;
//The projectiles that will be released back into the bullet pool
extern std::set<cGameObject*> ballsScheduledForRemoval;


//ctor
cBullet::cBullet()
{
	//if the bullet is in a object pool or not
	this->inPool = false;
}
//dtor
cBullet::~cBullet()
{
	physicsBody->GetWorld()->DestroyBody(physicsBody);
}

/*
*  Name:		Update()
*  Parameters:	double for the time
*  Summary:		Updates the bullet position based on time
*  Returns:		nothing
*/
void cBullet::Update(double deltaTime)
{
	//The position of the bullet
	b2Vec2 physicsPosition;
	//if the bullet has physics body
	if (physicsBody != 0)
	{
		physicsPosition = physicsBody->GetPosition();
		position.x = physicsPosition.x;
		position.z = physicsPosition.y;
	}// end if
}

/*
*  Name:		Contact
*  Parameters:	a pointer to a cGameObject
*  Summary:		is a method that is called when the bullet hits something
*  Returns:		nothing
*/
void cBullet::Contact(cGameObject * otherObject)
{
	
	//if the bullet is not in the pool
	if (!inPool) {
		cBulletPool::GetInstance()->Release(this);
	}// end if
}
