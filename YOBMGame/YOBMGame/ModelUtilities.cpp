#include "ModelUtilities.h" 
#include "cVAOMeshManager.h"
#include "cMesh.h"
#include <sstream>

/*
*  Name:		ReadFileToToken()
*  Parameters:	an input file stream, and a string token
*  Summary:		Reads through a file until it reaches a certain string
*  Returns:		nothing
*/
void ReadFileToToken(std::ifstream &file, std::string token)
{
	//if we should keep reading through the file
	bool bKeepReading = true;
	//holder variable for garbage
	std::string garbage;

	//read file until token is found
	do
	{
		file >> garbage;
		if (garbage == token)
		{
			return;
		}
	} while (bKeepReading);

	return;
}



/*
*  Name:		LoadPlyFileIntoMesh()
*  Parameters:	a string representing a file name, and a mesh to load data into
*  Summary:		loads mesh data into a mesh object
*  Returns:		a bool saying if it worked or not
*/
bool LoadPlyFileIntoMesh(std::string filename, cMesh &theMesh)
{
	// Load the vertices
	std::ifstream plyFile(filename.c_str());

	//if couldnt open the file
	if (!plyFile.is_open())
	{
		return false;
	}// end if

	// File is open, let's read it
	ReadFileToToken(plyFile, "vertex");
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken(plyFile, "face");
	plyFile >> theMesh.numberOfTriangles;

	ReadFileToToken(plyFile, "end_header");

	// Allocate the appropriate sized array (+a little bit)
	theMesh.pVertices = new cVertex[theMesh.numberOfVertices];
	theMesh.pTriangles = new cTriangle[theMesh.numberOfTriangles];

	//loop through the meshes vertices
	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{
		float x, y, z, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;

		theMesh.pVertices[index].x = x;
		theMesh.pVertices[index].y = y;
		theMesh.pVertices[index].z = z;
		theMesh.pVertices[index].r = 1.0f;
		theMesh.pVertices[index].g = 1.0f;
		theMesh.pVertices[index].b = 1.0f;
	}// end for

	//loop through all the triangles of the mesh
	for (int count = 0; count < theMesh.numberOfTriangles; count++)
	{
		int discard = 0;
		plyFile >> discard;		
		plyFile >> theMesh.pTriangles[count].vertex0ID;
		plyFile >> theMesh.pTriangles[count].vertex1ID;
		plyFile >> theMesh.pTriangles[count].vertex2ID;
	}

	theMesh.CalculateNormals();

	return true;
}

/*
*  Name:		LoadPlyFileIntoMeshWithNormals()
*  Parameters:	a string representing a file name, and a mesh to load data into
*  Summary:		loads mesh data into a mesh object, this file however should already have normals
*  Returns:		a bool saying if it worked or not
*/
bool LoadPlyFileIntoMeshWithNormals(std::string filename, cMesh &theMesh)
{
	// Load the vertices
	std::ifstream plyFile(filename.c_str());

	//if couldnt open the file
	if (!plyFile.is_open())
	{
		return false;
	}// end if

	// File is open, let's read it

	ReadFileToToken(plyFile, "vertex");
	plyFile >> theMesh.numberOfVertices;

	ReadFileToToken(plyFile, "face");
	plyFile >> theMesh.numberOfTriangles;

	ReadFileToToken(plyFile, "end_header");

	// Allocate the appropriate sized array (+a little bit)
	theMesh.pVertices = new cVertex[theMesh.numberOfVertices];
	theMesh.pTriangles = new cTriangle[theMesh.numberOfTriangles];

	//loop through all the meshes vertices
	for (int index = 0; index < theMesh.numberOfVertices; index++)
	{	
		//the position coordinates, the normal values, and the confidence and intensity
		float x, y, z, nx, ny, nz, confidence, intensity;

		plyFile >> x;
		plyFile >> y;
		plyFile >> z;
		plyFile >> nx >> ny >> nz;

		theMesh.pVertices[index].x = x;
		theMesh.pVertices[index].y = y;	
		theMesh.pVertices[index].z = z;
		theMesh.pVertices[index].r = 1.0f;	
		theMesh.pVertices[index].g = 1.0f;	
		theMesh.pVertices[index].b = 1.0f;	
		theMesh.pVertices[index].nx = nx;	
		theMesh.pVertices[index].ny = ny;	
		theMesh.pVertices[index].nz = nz;
	}

	// loop through all the meshes triangles
	for (int count = 0; count < theMesh.numberOfTriangles; count++)
	{
		int discard = 0;
		plyFile >> discard;								
		plyFile >> theMesh.pTriangles[count].vertex0ID;
		plyFile >> theMesh.pTriangles[count].vertex1ID;
		plyFile >> theMesh.pTriangles[count].vertex2ID;
	}
	return true;
}

/*
*  Name:		Load3DModelsIntoMeshManager()
*  Parameters:	an int representing the id of the current shader, a pointer to the VAO mesh manager
*				and an error string to change if something goes wrong
*  Summary:		loads 3d models into the mesh managers, reading from a text file
*  Returns:		a bool saying if it worked or not
*/
bool Load3DModelsIntoMeshManager(int shaderID, cVAOMeshManager* pVAOManager, std::string &error)
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	std::stringstream ssError;
	bool anyErrors = false;
	bool normals = false;

	try {
		//go through the file line by line
		while (getline(inFile, line))
		{
			//if the line is blank
			if (line == "")
			{
				continue;
			}// end if

			//if the line contains normals
			if (line.find("normals:") != std::string::npos) 
			{
				line.replace(0, 9, "");
				normals = line == "true";
				continue;
			}// end if

			//if the line contains filename_l
			if (line.find("filename_l:") != std::string::npos) 
			{
				line.replace(0, 12, "");
				cMesh testMesh;
				testMesh.name = line;
				//if the file has normals
				if (normals) 
				{
					//if the file didnt load
					if (!LoadPlyFileIntoMeshWithNormals(line, testMesh))
					{
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						anyErrors = true;
					}// end if
				}// end if
				else
				{
					//if the file didnt load
					if (!LoadPlyFileIntoMesh(line, testMesh))
					{
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						anyErrors = true;
					}// end if
				}

				//if the file didnt load into the vao
				if (!pVAOManager->LoadMeshIntoVAO(testMesh, shaderID))
				{
					ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
					anyErrors = true;
				}// end if
				continue;
			}// end if

			//if the line included filename
			if (line.find("filename:") != std::string::npos) 
			{
				line.replace(0, 10, "");
				cMesh testMesh;
				testMesh.name = line;
				//if the file contains normals
				if (normals) 
				{
					//if the file didnt load into the mesh
					if (!LoadPlyFileIntoMeshWithNormals(line, testMesh))
					{
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						anyErrors = true;
					}// end if
				}// end if
				else
				{
					//if the file didnt load into the mesh
					if (!LoadPlyFileIntoMesh(line, testMesh))
					{
						ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;
						anyErrors = true;
					}// end if
				}// end if

				//if the mesh didnt load into the vao
				if (!pVAOManager->LoadMeshIntoVAO(testMesh, shaderID))
				{
					ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;
					anyErrors = true;
				}// end if
				continue;
			}// end if
		}

	}
	catch (...)
	{
		return true;
	}

	//if there were any errors
	if (!anyErrors)
	{
		error = ssError.str();
	}// end if
	return anyErrors;

}
