#include "cSoundEngine.h"
#include <vector>
#include <fstream>
#include <string>

//The global container of souns
extern std::vector< cSoundEngine* > g_vecSoundObjects;


/*
*  Name:		LoadAllSounds()
*  Parameters:	nothing
*  Summary:		loads the sounds from a file
*  Returns:		a bool saying if it worked or not
*/
void LoadAllSounds()
{
	std::string filename = "SoundConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	std::string fName;
	std::string soundName;
	int soundCounter = 0;
	try {
		//while there are lines in the file to read
		cSoundEngine *mySound;
		while (getline(inFile, line))
		{
			//if the line is blank

			if (line == "")
			{
				continue;
			}// end if

			 //if the line contains Object Start
			if (line == "Sound Start")
			{
				soundCounter++;
				mySound = new cSoundEngine();
				continue;
			}// end if

			if (line.find("filepath:") != std::string::npos)
			{
				line.replace(0, 10, "");
				fName = line;
				mySound->createSound(fName);
				continue;
			}
			if (line.find("name:") != std::string::npos)
			{
				line.replace(0, 6, "");
				soundName = line;
				mySound->nameOfSound = soundName;
				mySound->playSound();
				if (soundName != "background")
				{
					mySound->pauseSound(true);
				}
				::g_vecSoundObjects.push_back(mySound);
				continue;
			}
			if (line == "Sound End")
			{
				continue;
			}// end if




		}
	}
	catch (std::exception ex)
	{
		return;
	}
}

