// C/C++ Libraries
#include <fstream>
#include <string>
#include <vector>

// Json Library provided by nlohmann on github
#include "include/nlohmann/json.hpp"

// Project Includes
#include "JsonUtilities.h"
#include "ModelUtilities.h"
#include "cVAOMeshManager.h"
#include "cMesh.h"
#include "cGameObject.h"
#include "cGameObjectFactory.h"
#include "cLightManager.h"

// Externs
extern std::vector< cGameObject* >  g_vecGameObjects;

/*
*  Defines for consisteny and easy to modify later
*  These are the naming conventions that the .json file needs to follow
*/

// JSON Other Property Defines
#define YOBM_JU_BASEPATH "basePath"
#define YOBM_JU_CAMERA_POSITION "cameraXYZ"
#define YOBM_JU_CAMERA_TARGET "cameraTarget_XYZ"

// JSON Model Files Defines
#define YOBM_JU_MODELFILE_CONTAINER "ModelFiles"
#define YOBM_JU_MODELFILE "ModelFile"
#define YOBM_JU_MODELFILE_MESHNAME "meshName"
#define YOBM_JU_MODELFILE_FILENAME "fileName"
#define YOBM_JU_GAMEOBJECT_NORMALS "normals"

// JSON GameObject Defines
#define YOBM_JU_GAMEOBJECT_CONTAINER "cGameObjects"
#define YOBM_JU_GAMEOBJECT "GameObject"
#define YOBM_JU_GAMEOBJECT_MESHNAME "meshName"
#define YOBM_JU_GAMEOBJECT_SCALE "scale"
#define YOBM_JU_GAMEOBJECT_WIREFRAME "isWireFrame"
#define YOBM_JU_GAMEOBJECT_PHYSICS "physics"
#define YOBM_JU_GAMEOBJECT_POSITION "position"
#define YOBM_JU_GAMEOBJECT_COLOR "color"
#define YOBM_JU_GAMEOBJECT_ORIENTATION_PRE "orientation"
#define YOBM_JU_GAMEOBJECT_ORIENTATION_POST "orientation2"
#define YOBM_JU_GAMEOBJECT_TYPE "type"

// JSON Light Defines
#define YOBM_JU_LIGHT_CONTAINER "cLights"
#define YOBM_JU_LIGHT "Light"
#define YOBM_JU_LIGHT_POSITION "position"
#define YOBM_JU_LIGHT_COLOR "color"
#define YOBM_JU_LIGHT_ATTENUATION "attenuation"

/*
*  Name:		VectorToVec3()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec3
*  Returns:		glm::vec3
*/
glm::vec3 VectorToVec3(std::vector<float> vec)
{
	// Split the vector of floats into the vec3
	return glm::vec3(vec[0], vec[1], vec[2]);
}

/*
*  Name:		VectorToVec3Radians()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec3 with radian values
*  Returns:		glm::vec3
*/
glm::vec3 VectorToVec3Radians(std::vector<float> vec)
{
	// Split the vector of floats into the vec3 and convert to radians
	return glm::vec3(glm::radians(vec[0]), glm::radians(vec[1]), glm::radians(vec[2]));
}

/*
*  Name:		VectorToVec4()
*  Parameters:	vector of type float
*  Summary:		Helper method that converts a vector of floats into a glm::vec4
*  Returns:		glm::vec4
*/
glm::vec4 VectorToVec4(std::vector<float> vec)
{
	// Split the vector of floats into the vec4
	return glm::vec4(vec[0], vec[1], vec[2], vec[3]);
}

/*
*  Name:		LoadCameraFromJSON()
*  Parameters:	string containing file to load, glm::vec3 reference to camera positition, glm::vec3 reference to camera target
*  Summary:		Retrieve camera configurations from json file and apply them to referenced camera
*  Returns:		void
*/
void LoadCameraFromJSON(std::string jsonFile, glm::vec3 &camera, glm::vec3 &cameraTarget)
{
	// Open file 
	std::ifstream inFile(jsonFile);

	// Create json Object
	nlohmann::json jsonMaster;

	// Inject file into json Object
	inFile >> jsonMaster;

	// Get the Camera Properties from json and set them to the referenced camera vec3's
	camera = VectorToVec3(jsonMaster.at(YOBM_JU_CAMERA_POSITION).get<std::vector<float>>());
	cameraTarget = VectorToVec3(jsonMaster.at(YOBM_JU_CAMERA_TARGET).get<std::vector<float>>());
}

/*
*  Name:		LoadModelFilesFromJSON()
*  Parameters:	string containing file to load, int representing the shader id, object pointer to the VAO Manager, string for writting errors
*  Summary:		Retrieves the model files from json file to be loaded into the GPU
*  Returns:		void
*/
bool LoadModelFilesFromJSON(std::string jsonFile, int shaderID, cVAOMeshManager* pVAOManager, std::string &error)
{
	// Error string stream for output when errors are detected
	std::stringstream ssError;

	// Error boolean that communicates if errors occured at all 
	bool bAnyErrors = false;

	// Meshnames that will be used for lookup by GameObjects
	std::vector<std::string> vecMeshNames;

	// Filenames of the model files to be loaded into VRAM
	std::vector<std::string> vecFileNames;

	// Open file
	std::ifstream inFile(jsonFile);

	// Create json object
	nlohmann::json jsonMaster;

	// Injest file data into json object
	inFile >> jsonMaster;

	// Get number of files to read in 
	int numFiles = jsonMaster[YOBM_JU_MODELFILE_CONTAINER].size();

	// Get the base path
	std::string basePath = jsonMaster.at(YOBM_JU_BASEPATH).get<std::string>();

	// Get the files
	for (int index = 0; index < numFiles; index++)
	{
		// The name of the object being read
		std::string objName = YOBM_JU_MODELFILE;

		// Index appended to the object name to designate the json object being read
		std::string str = std::to_string(index + 1);
		objName.append(str);

		// Read object into the parallel vectors
		vecMeshNames.push_back(jsonMaster[YOBM_JU_MODELFILE_CONTAINER][objName][YOBM_JU_MODELFILE_MESHNAME].get<std::string>());
		vecFileNames.push_back(jsonMaster[YOBM_JU_MODELFILE_CONTAINER][objName][YOBM_JU_MODELFILE_FILENAME].get<std::string>());
	}

	// Load the files into the VAO Manager
	int numMeshes = vecMeshNames.size();
	for (int index = 0; index < numMeshes; index++)
	{
		// Temp Mesh Object
		cMesh testMesh;

		// Set the Name of the mesh being loaded
		testMesh.name = vecMeshNames[index];

		// Load file into Mesh
		if (!LoadPlyFileIntoMeshWithNormals(basePath + vecFileNames[index], testMesh))
		{
			// If failed, store the name of the mesh that failed
			ssError << "Didn't load model >" << testMesh.name << "<" << std::endl;

			// Inform that there are some errors
			bAnyErrors = true;
		}

		// Load Mesh into VAO
		if (!pVAOManager->LoadMeshIntoVAO(testMesh, shaderID))
		{
			// If failed, store the name of the mesh that failed
			ssError << "Could not load mesh >" << testMesh.name << "< into VAO" << std::endl;

			// Inform that there are some errors
			bAnyErrors = true;
		}
	}

	// Report if errors occured and return the error string stream
	if (!bAnyErrors)
	{
		// Copy the error string stream into the error string that
		//	gets "returned" (through pass by reference)
		error = ssError.str();
	}

	return bAnyErrors;
}

/*
*  Name:		LoadGameObjectsFromJSON()
*  Parameters:	string containing file to load
*  Summary:		Reads the json file and creates the GameObjects in the scene and pushes them to the extern vector of GameObjects
*  Returns:		void
*/
void LoadGameObjectsFromJSON(std::string jsonFile)
{
	// Open file
	std::ifstream inFile(jsonFile);

	// Create Json Object
	nlohmann::json jsonMaster;

	// Inject file data into json object
	inFile >> jsonMaster;

	// Retrieve GameObjects from json 
	int numberOfGameObjects = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER].size();
	for (int index = 0; index < numberOfGameObjects; index++)
	{
		// The name of the object being read
		std::string objName = YOBM_JU_GAMEOBJECT;

		// Append the index to the object name to indicate which object is being referenced
		std::string str = std::to_string(index + 1);
		objName.append(str);

		// Create cGameObject using factory
		cGameObject* pTempGO = cGameObjectFactory::GetInstance()->CreateGameObject(jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_TYPE].get<std::string>());

		// Read json object into a cGameObject
		pTempGO->meshName = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_MESHNAME].get<std::string>();
		pTempGO->scale = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_SCALE].get<float>();
		pTempGO->isWireFrame = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_WIREFRAME].get<bool>();
		pTempGO->isUpdatedInPhysics = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_PHYSICS].get<bool>();
		//pTempGO->typeOfObject = jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_TYPE].get<std::string>();

		// Need to Convert these elements to glm::vec3 and vec4s
		pTempGO->position = VectorToVec3(jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_POSITION].get<std::vector<float>>());
		pTempGO->diffuseColour = VectorToVec4(jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_COLOR].get<std::vector<float>>());
		pTempGO->orientation = VectorToVec3Radians(jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_ORIENTATION_PRE].get<std::vector<float>>());
		pTempGO->orientation2 = VectorToVec3Radians(jsonMaster[YOBM_JU_GAMEOBJECT_CONTAINER][objName][YOBM_JU_GAMEOBJECT_ORIENTATION_POST].get<std::vector<float>>());

		// Push the GameObject
		g_vecGameObjects.push_back(pTempGO);
	}

}

/*
*  Name:		GetNumLightsFromJSON()
*  Parameters:	string containing file to load, reference to int passed to LightManager that creates the lights
*  Summary:		Reads the json file and counts the number of lights defined in the file
*  Returns:		void
*/
void GetNumLightsFromJSON(std::string jsonFile, int &numLights)
{
	// Open File
	std::ifstream inFile(jsonFile);

	// Create Json Object
	nlohmann::json jsonMaster;

	// Inject file data into json object
	inFile >> jsonMaster;

	// Get the number of lights from json
	numLights = jsonMaster[YOBM_JU_LIGHT_CONTAINER].size();

}

/*
*  Name:		LoadLightsFromJSON()
*  Parameters:	string containing file to load, reference to the vector that stores the Light Objects
*  Summary:		Reads the json file and modifies the properties of the created lights in the vector of lights
*  Returns:		void
*/
void LoadLightsFromJSON(std::string jsonFile, std::vector<cLight> &vecLights)
{
	// Open file
	std::ifstream inFile(jsonFile);
	
	// Create json object
	nlohmann::json jsonMaster;

	// Inject file data into json object 
	inFile >> jsonMaster;

	// Get the cLights
	int numLights = jsonMaster[YOBM_JU_LIGHT_CONTAINER].size();
	for (int index = 0; index < numLights; index++)
	{
		// The name of the object being read
		std::string objName = YOBM_JU_LIGHT;

		// Append the index to the object name to indicate which object is being read in the json
		std::string str = std::to_string(index + 1);
		objName.append(str);

		// Convert attributes to glm::vec3 and vec4s and store them in Light object 
		vecLights[index].position = VectorToVec3(jsonMaster[YOBM_JU_LIGHT_CONTAINER][objName][YOBM_JU_LIGHT_POSITION].get<std::vector<float>>());
		vecLights[index].diffuse = VectorToVec4(jsonMaster[YOBM_JU_LIGHT_CONTAINER][objName][YOBM_JU_LIGHT_COLOR].get<std::vector<float>>());
		vecLights[index].attenuation = VectorToVec3(jsonMaster[YOBM_JU_LIGHT_CONTAINER][objName][YOBM_JU_LIGHT_ATTENUATION].get<std::vector<float>>());
	}

}
