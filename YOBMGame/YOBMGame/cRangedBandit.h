#ifndef _cRangedBandit_HG_
#define _cRangedBandit_HG_
#include "cGameObject.h"

/*
*  Name:		cRangedBandit
*  Summary:		Represents a bandit that will try to attack Ye Olde Bazooka Man
*  Impliments:	cGameObject
*/
class cRangedBandit : public cGameObject
{
public:
	//ctor
	cRangedBandit();
	//dtor
	~cRangedBandit();

	// whether or not the bandit is stunned
	bool stunned;

	// the time remaining of being stunned
	int stunTime;

	// the maximum amount of time the bandit can be stunned
	int stunTimeReset;

	// whether or not the bandit is facing the player
	bool facingPlayer;

	// a timer counting down until the bandit can shoot again
	int shootTimer;

	// the time between shots
	int shootTimerReset;
	/*
	*  Name:		Update
	*  Parameters:	double for the time
	*  Summary:		Controls the bandits behavior
	*  Returns:		nothing
	*/
	virtual void Update(double deltaTime);

	/*
	*  Name:		Contact
	*  Parameters:	a cGameObject pointer of the game object this object
	*				has made contact with
	*  Summary:		a method that is called when this object
	*				makes contact with another object
	*  Returns:		nothing
	*/
	virtual void Contact(cGameObject* otherObject);

	/*
	*  Name:		Fire
	*  Parameters:	nothing
	*  Summary:		makes the bandit fire a bullet
	*  Returns:		nothing
	*/
	void Fire();
};

#endif // !_cRangedBandit_HG_

