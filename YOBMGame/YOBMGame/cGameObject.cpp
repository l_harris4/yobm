#include "cGameObject.h"


//ctor
cGameObject::cGameObject()
{
	//the scale of the object
	this->scale = 1.0f;
	//the position of the object
	this->position = glm::vec3(0.0f);
	//the pre rotation of the object
	this->orientation = glm::vec3(0.0f);
	//the post rotation of the object
	this->orientation2 = glm::vec3(0.0f);
	//the velocity of the object
	this->vel = glm::vec3(0.0f);
	//the acceleration of the object
	this->accel = glm::vec3(0.0f);
	//the colour of the object
	this->diffuseColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	//the original colour of the object
	this->originalColour = diffuseColour;
	//the world matrix of the model
	this->mModel = glm::mat4x4(1.0f);
	//the velocity of the object last frame
	this->holderVel = glm::vec3(0.0f);
	//if the object can be moved from the starting position
	this->editable = false;
	//if the object is updated by the physics step
	this->isUpdatedInPhysics = true;
	//if the object is affected by gravity
	this->affectedByGravity = false;
	//the radius of the object
	this->radius = 0.0f;
	//if the object has collided this frame
	collidedThisFrame = false;
	//if the object is wireframe
	this->isWireFrame = false;
	//the physics body of the object
	this->physicsBody = 0;
	return;
}

//dtor
cGameObject::~cGameObject()
{
	return;
}

/*
*  Name:		OrientAngle
*  Parameters:	a vec2 representing the velocity
*  Summary:		Orients the game object based on velocity
*  Returns:		returns an orientation angle
*/
float cGameObject::OrientAngle(glm::vec2 a) {
	float angle = atan2(a.x, a.y);
	return angle;
}

/*
*  Name:		Update()
*  Parameters:	double for the time
*  Summary:		Updates the game object position based on time
*  Returns:		nothing
*/
void cGameObject::Update(double deltaTime)
{
}

/*
*  Name:		PerformAction
*  Parameters:	nothing
*  Summary:		a general method used to make an object perform
*				its 'function'
*  Returns:		nothing
*/
void cGameObject::PerformAction()
{
}

/*
*  Name:		Contact
*  Parameters:	a cGameObject pointer of the game object this object
*				has made contact with
*  Summary:		a general method that is called when this object
*				makes contact with another object
*  Returns:		nothing
*/
void cGameObject::Contact(cGameObject * otherObject)
{
}
