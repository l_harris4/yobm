#include "cPhysicsListener.h"
#include "cGameObject.h"
#include <iostream>

//ctor
cPhysicsListener::cPhysicsListener()
{
}

//dtor
cPhysicsListener::~cPhysicsListener()
{
}

/*
*  Name:		BeginContact
*  Parameters:	a pointer to a contact
*  Summary:		starts contact with another object
*  Returns:		nothing
*/
void cPhysicsListener::BeginContact(b2Contact* contact)
{
	//std::cout << "Begin contact" << std::endl;
	try {
		//get pointers to the objects contacting
		cGameObject* goA = (cGameObject*)contact->GetFixtureA()->GetBody()->GetUserData();
		cGameObject* goB = (cGameObject*)contact->GetFixtureB()->GetBody()->GetUserData();

		//if the objects are ok
		if (goB != nullptr && goA != nullptr)
		{
			//call contact on both of them
			goA->Contact(goB);
			goB->Contact(goA);
		}
	}
	catch (...)
	{

	}
}

/*
*  Name:		EndContact
*  Parameters:	a pointer to a contact
*  Summary:		ends contact with another object
*  Returns:		nothing
*/
void cPhysicsListener::EndContact(b2Contact* contact)
{
	//std::cout << "End contact" << std::endl;
}