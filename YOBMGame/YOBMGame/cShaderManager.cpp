#include "cShaderManager.h"

#include <glad/glad.h>

#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>

//ctor
cShaderManager::cShaderManager()
{
	return;
}

//dtor
cShaderManager::~cShaderManager()
{
	return;
}

/*
*  Name:		UseShaderProgram()
*  Parameters:	an int being the name of the shader
*  Summary:		Tells the shader manager what is the current shader
*  Returns:		a bool saying if this was successfully done or not
*/
bool cShaderManager::UseShaderProgram(unsigned int ID)
{
	glUseProgram(ID);
	return true;
}

/*
*  Name:		UseShaderProgram()
*  Parameters:	a string being the name of the shader
*  Summary:		Tells the shader manager what is the current shader
*  Returns:		a bool saying if this was successfully done or not
*/
bool cShaderManager::UseShaderProgram(std::string friendlyName)
{
	std::map< std::string, unsigned int >::iterator
		itShad = this->nameToID.find(friendlyName);

	//if couldn't find the shader
	if (itShad == this->nameToID.end())
	{
		return false;
	}// end if

	glUseProgram(itShad->second);

	return true;
}

/*
*  Name:		GetIDFromFriendlyName()
*  Parameters:	a string representing the friendly name of the shader
*  Summary:		gets the id of a shader from the friendly name
*  Returns:		the id of the shader
*/
unsigned int cShaderManager::GetIDFromFriendlyName(std::string friendlyName)
{
	std::map< std::string, unsigned int >::iterator
		itShad = this->nameToID.find(friendlyName);

	//if couldn't find the shader
	if (itShad == this->nameToID.end())
	{	
		return 0;
	}// end if

	return itShad->second;
}

const unsigned int MAXLINELENGTH = 65536;


/*
*  Name:		SetBasePath()
*  Parameters:	a string representing the new base path
*  Summary:		sets where the files are stored
*  Returns:		nothing
*/
void cShaderManager::SetBasePath(std::string basepath)
{
	this->basePath = basepath;
	return;
}


/*
*  Name:		LoadSourceFromFile
*  Parameters:	a cShader object
*  Summary:		loads the appropriate shader info in from a file to the cShader object
*  Returns:		a bool saying if this was successful or not
*/
bool cShaderManager::LoadSourceFromFile(cShader &shader)
{
	std::string fullFileName = this->basePath + shader.fileName;

	std::ifstream theFile(fullFileName.c_str());

	//if couldn't open the file
	if (!theFile.is_open())
	{
		return false;
	}// end if

	shader.vecSource.clear();

	char pLineTemp[MAXLINELENGTH] = { 0 };
	//go through the file line by line
	while (theFile.getline(pLineTemp, MAXLINELENGTH))
	{
		std::string tempString(pLineTemp);
		shader.vecSource.push_back(tempString);
	}// end while

	theFile.close();
	return true;		// Return the string (from the sstream)
}

/*
*  Name:		WasThereACompileError
*  Parameters:	an int representing a shader id, and a string where we would store
*  Summary:		determines if a compile error happened or not
*  Returns:		a bool saying if there was a compile error or not
*/
bool cShaderManager::WasThereACompileError(unsigned int shaderID, std::string &errorText)
{
	errorText = "";

	GLint isCompiled = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);

	//if it didn't compile
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);

		char* pLogText = new char[maxLength];

		glGetShaderInfoLog(shaderID, maxLength, &maxLength, pLogText);
		// Copy char array to string
		errorText.append(pLogText);

		this->lastError.append("\n");
		this->lastError.append(errorText);


		delete[] pLogText;

		return true;
	}// end if
	return false; // There WASN'T an error
}

/*
*  Name:		WasThereALinkError
*  Parameters:	an int representing a shader id, and a string where we would store
*  Summary:		determines if a link error happened or not
*  Returns:		a bool saying if there was a link error or not
*/
bool cShaderManager::WasThereALinkError(unsigned int programID, std::string &errorText)
{
	errorText = "";

	GLint wasntError = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &wasntError);

	//if there was an error
	if (wasntError == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

		char* pLogText = new char[maxLength];
		// Fill with zeros, maybe...?
		glGetProgramInfoLog(programID, maxLength, &maxLength, pLogText);
		// Copy char array to string
		errorText.append(pLogText);

		this->lastError.append("\n");
		this->lastError.append(errorText);

		delete[] pLogText;

		// There WAS an error
		return true;
	}// end if

	// There WASN'T an error
	return false;
}

/*
*  Name:		GetLastError()
*  Parameters:	nothing
*  Summary:		gets the last error that occured
*  Returns:		a string representing the error
*/
std::string cShaderManager::GetLastError(void)
{
	std::string lastErrorTemp = this->lastError;
	this->lastError = "";
	return lastErrorTemp;
}


/*
*  Name:		CompileShaderFromSource
*  Parameters:	a cShader object, and a string where we would store an error
*  Summary:		attempts to compile a shader
*  Returns:		a bool saying if this was successful or not
*/
bool cShaderManager::CompileShaderFromSource(cShaderManager::cShader &shader, std::string &error)
{
	error = "";

	const unsigned int MAXLINESIZE = 8 * 1024;	// About 8K PER LINE

	unsigned int numberOfLines = static_cast<unsigned int>(shader.vecSource.size());

	char** arraySource = new char*[numberOfLines];
	memset(arraySource, 0, numberOfLines);

	//go through all the lines
	for (unsigned int indexLine = 0; indexLine != numberOfLines; indexLine++)
	{
		unsigned int numCharacters = (unsigned int)shader.vecSource[indexLine].length();
		// Create an array of chars for each line
		arraySource[indexLine] = new char[numCharacters + 2];
		memset(arraySource[indexLine], 0, numCharacters + 2);

		//for each character in the line
		for (unsigned int indexChar = 0; indexChar != shader.vecSource[indexLine].length(); indexChar++)
		{
			arraySource[indexLine][indexChar] = shader.vecSource[indexLine][indexChar];
		}// end for

		arraySource[indexLine][numCharacters + 0] = '\n';
		arraySource[indexLine][numCharacters + 1] = '\0';

	}// end for

	glShaderSource(shader.ID, numberOfLines, arraySource, NULL);
	glCompileShader(shader.ID);

	// Get rid of the temp source "c" style array
	for (unsigned int indexLine = 0; indexLine != numberOfLines; indexLine++)
	{	// Delete this line
		delete[] arraySource[indexLine];
	}// end for

	// And delete the original char** array
	delete[] arraySource;

	std::string errorText = "";
	//if there was a compile error
	if (this->WasThereACompileError(shader.ID, errorText))
	{
		std::stringstream ssError;
		ssError << shader.GetShaderTypeString();
		ssError << " compile error: ";
		ssError << errorText;
		error = ssError.str();
		return false;
	}// end if

	return true;
}


/*
*  Name:		CreateProgramFromFile()
*  Parameters:	a string being the name of the shader, a shader object for the vertext shader, and a shader object for the fragment shader
*  Summary:		creates a shader program object
*  Returns:		a bool saying if this was successfully done or not
*/
bool cShaderManager::CreateProgramFromFile(std::string friendlyName, cShader &vertexShad, cShader &fragShader)
{
	std::string errorText = "";

	// Shader loading happening before vertex buffer array
	vertexShad.ID = glCreateShader(GL_VERTEX_SHADER);
	vertexShad.shaderType = cShader::VERTEX_SHADER;

	// if the source wasnt loaded from the file
	if (!this->LoadSourceFromFile(vertexShad))
	{
		return false;
	}// end if

	errorText = "";
	//if the source didnt compile
	if (!this->CompileShaderFromSource(vertexShad, errorText))
	{
		this->lastError = errorText;
		return false;
	}//end if

	fragShader.ID = glCreateShader(GL_FRAGMENT_SHADER);
	fragShader.shaderType = cShader::FRAGMENT_SHADER;
	//if the source didn't load from file
	if (!this->LoadSourceFromFile(fragShader))
	{
		return false;
	}// end if

	//if the source didnt compile
	if (!this->CompileShaderFromSource(fragShader, errorText))
	{
		this->lastError = errorText;
		return false;
	}// end if


	cShaderProgram curProgram;
	curProgram.ID = glCreateProgram();

	glAttachShader(curProgram.ID, vertexShad.ID);
	glAttachShader(curProgram.ID, fragShader.ID);
	glLinkProgram(curProgram.ID);

	errorText = "";
	//if there was a link error
	if (this->WasThereALinkError(curProgram.ID, errorText))
	{
		std::stringstream ssError;
		ssError << "Shader program link error: ";
		ssError << errorText;
		this->lastError = ssError.str();
		return false;
	}// end if

	// At this point, shaders are compiled and linked into a program
	curProgram.friendlyName = friendlyName;

	// Add the shader to the map
	this->IDToShader[curProgram.ID] = curProgram;
	// Save to other map, too
	this->nameToID[curProgram.friendlyName] = curProgram.ID;

	return true;
}
