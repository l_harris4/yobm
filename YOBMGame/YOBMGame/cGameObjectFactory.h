#ifndef _cGameObjectFactory_HG_
#define _cGameObjectFactory_HG_

#include "cGameObject.h"
#include <string>
#include <vector>

/*
*  Name:		cGameObjectFactory
*  Summary:		Used to create game objects
*  Impliments:	nothing
*/
class cGameObjectFactory
{
public:
	/*
	*  Name:		CreateGameObject()
	*  Parameters:	a string representing the object type
	*  Summary:		Creates a new game object of a certain type
	*  Returns:		cGameObject*
	*/
	cGameObject* CreateGameObject(std::string objectType);

	/*
	*  Name:		GetInstance()
	*  Parameters:	nothing
	*  Summary:		gets the actual instance of the factory
	*  Returns:		cGameObjectFactory*
	*/
	static cGameObjectFactory* GetInstance();

private:
	//the private instance of the actual factory object
	static cGameObjectFactory* instance;
	//private constructor so no other class can create one
	cGameObjectFactory() {};
};

#endif // !_cGameObjectFactory_HG_


