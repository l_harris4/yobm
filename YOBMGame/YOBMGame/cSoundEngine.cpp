#include "cSoundEngine.h"
#include <stdio.h>
#include <Windows.h>

int cSoundEngine::m_nextID = 1;
//Constructor
cSoundEngine::cSoundEngine()
{
	system = NULL;
	nameOfSound = "";
	this->m_uniqueID = cSoundEngine::m_nextID;
	cSoundEngine::m_nextID++;

	// Create the main system object.
	result = FMOD::System_Create(&system);
	if (result != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}

	//Initializes the system object, and the msound device. This has to be called at the start of the user's program
	result = system->init(100, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	if (result != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}
}

//Destructor
cSoundEngine::~cSoundEngine()
{
	if (system) {
		result = system->close();
		errorCheck(result);
		result = system->release();
		errorCheck(result);
	}
}

/*
*  Name:		getID()
*  Parameters:	nothing
*  Summary:		get a unique id for a sound object
*  Returns:		nothing
*/
int cSoundEngine::getID(void)
{
	return this->m_uniqueID = 1;
}


/*
*  Name:		errorCheck()
*  Parameters:	FMOD_RESULT
*  Summary:		checks for any errors after performing sound related task
*  Returns:		nothing
*/
void cSoundEngine::errorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		fprintf(stderr, "FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(EXIT_FAILURE);
	}
}


/*
*  Name:		createSound()
*  Parameters:	name of a file as string
*  Summary:		is a method to create a sound
*  Returns:		nothing
*/
void cSoundEngine::createSound(std::string fileName)
{
	this->result = this->system->createSound(fileName.c_str(), FMOD_DEFAULT, 0, &this->sound);
	this->errorCheck(result);
}

/*
*  Name:		playSound()
*  Parameters:	nothing
*  Summary:		is a method to play a sound
*  Returns:		nothing
*/
void cSoundEngine::playSound()
{
 	this->result = this->system->playSound(this->sound, 0, false, &this->channel);
	this->errorCheck(result);
}


/*
*  Name:		pauseSound()
*  Parameters:	boolean state
*  Summary:		is a method to toggle pause state of a channel
*  Returns:		nothing
*/
void cSoundEngine::pauseSound(bool pauseState)
{
	this->result = this->channel->setPaused(pauseState);
	this->errorCheck(result);
}