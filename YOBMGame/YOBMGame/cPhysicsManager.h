#ifndef PHYSICS_MANAGER_HG
#define PHYSICS_MANAGER_HG

#include <Box2D\Box2D.h>

/*
*  Name:		cPhysicsManager
*  Summary:		manages the physics in the world, ie: gravity/velocity
*  Impliments:	nothing
*/
class cPhysicsManager
{
public:
	// ctor
	cPhysicsManager();
	// dtor
	~cPhysicsManager();

	/*
	*  Name:		StartUp
	*  Parameters:	nothing
	*  Summary:		starts the physics simulation of the world
	*  Returns:		nothing
	*/
	void StartUp();

	/*
	*  Name:		Shutdown
	*  Parameters:	nothing
	*  Summary:		stops the physics simulation of the world
	*  Returns:		nothing
	*/
	void Shutdown();

	/*
	*  Name:		Update
	*  Parameters:	passed time in a float
	*  Summary:		Updates the objects in the world based on time
	*				and velocity
	*  Returns:		nothing
	*/
	void Update(float deltaTime);

	/*
	*  Name:		SetGravity
	*  Parameters:	vec2 representing gravity
	*  Summary:		sets the gravity vector of the world
	*  Returns:		nothing
	*/
	void SetGravity(b2Vec2 gravity);

	/*
	*  Name:		SetContactListener
	*  Parameters:	a contact listener pointer
	*  Summary:		sets the contact listener of the world
	*  Returns:		nothing
	*/
	void SetContactListener(b2ContactListener* contactListener);

	/*
	*  Name:		CreateBody
	*  Parameters:	a const pointer to a body 
	*  Summary:		creates a physics object in the world
	*  Returns:		returns a pointer to a physics body that it creates
	*/
	b2Body* CreateBody(const b2BodyDef* def);

	/*
	*  Name:		DestroyBody
	*  Parameters:	a const pointer to a body
	*  Summary:		removes a physics body from the world
	*  Returns:		nothing
	*/
	void DestroyBody(b2Body* body);

private:
	// the gravity of the world
	b2Vec2 mGravity;

	// the pointer to the actual world
	b2World* mPhysicsWorld;

	// how many velocity iterations per frame
	int32 mVelocityIterations;

	// how many position iterations per frame
	int32 mPositionIterations;
};

#endif
