#include "cVertex.h"

float x = 0.0f;

// Constructor
cVertex::cVertex()
{
	//the position coordinates
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
	//the colour values
	this->r = 0.0f;
	this->g = 0.0f;
	this->b = 0.0f;
	//the values of the normals
	this->nx = 0.0f;
	this->ny = 0.0f;
	this->nz = 0.0f;
	return;
}

// Destructor
cVertex::~cVertex()
{
	// Doesn't really do anything
	return;
}
