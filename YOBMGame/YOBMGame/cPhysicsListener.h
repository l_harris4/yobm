#ifndef _PhysicsListener_HG_
#define _PhysicsListener_HG_

#include <Box2D\Box2D.h>
/*
*  Name:		cPhysicsListener
*  Summary:		to listen for physics collisions
*  Impliments:	b2ContactListener
*/
class cPhysicsListener : public b2ContactListener
{
public:
	// ctor
	cPhysicsListener();
	// dtor
	~cPhysicsListener();

	/*
	*  Name:		BeginContact
	*  Parameters:	a pointer to a contact
	*  Summary:		starts contact with another object
	*  Returns:		nothing
	*/
	virtual void BeginContact(b2Contact* contact);

	/*
	*  Name:		EndContact
	*  Parameters:	a pointer to a contact
	*  Summary:		ends contact with another object
	*  Returns:		nothing
	*/
	virtual void EndContact(b2Contact* contact);
};

#endif //!_PhysicsListener_HG_

