#ifndef _cLightManager_HG_
#define _cLightManager_HG_

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <vector>

/*
*  Name:		cLight
*  Summary:		Used to represent a light source
*  Impliments:	nothing
*/
class cLight
{
public:
	// ctor
	cLight();

	// data members

	// position of light in 3D world
	glm::vec3 position;

	// colour of light 
	glm::vec3 diffuse;

	// amount of ambient light
	glm::vec3 ambient;

	// shininess of the light
	glm::vec4 specular;

	// the intensity and falloff value of the light
	glm::vec3 attenuation;

	// the target of the light, used for spot lights
	glm::vec3 direction;

	// defines the type of light, spotlight, point light etc.
	glm::vec4 typeParams;

	// name of file that the light is loaded from
	std::string fileName;

	// index of the game object that "has" this light
	int gameObjectIndex;

	// uniform locations for the shader
	int shaderLocPosition;
	int shaderLocDiffuse;
	int shaderLocAmbient;
	int shaderLocSpecular;
	int shaderLocAttenuation;
	int shaderLocDirection;
	int shaderLocTypeParams;

	// Constants
	// int defined here because it can be, but floats defined in cpp
	static const unsigned int DEFAULTMAXITERATIONS = 50;
	static const float DEFAULTINFINITEDISTANCE;
	static const float DEFAULDIFFUSEACCURACYTHRESHOLD;
	static const float DEFAULTZEROTHRESHOLD;

	/*
	*  Name:		CalcApproxDistFromAtten()
	*  Parameters:	a float representing the wanted light level
	*  Summary:		To get the distance from a light if we want a certain attentuation
	*  Returns:		a float representing the distance away from the light
	*/
	float CalcApproxDistFromAtten(float targetLightLevel);

	/*
	*  Name:		CalcApproxDistFromAtten()
	*  Parameters:	a float representing the wanted light level, a float representing the desired accuracy of this function
	*  Summary:		To get the distance from a light if we want a certain attentuation
	*  Returns:		a float representing the distance away from the light
	*/
	float CalcApproxDistFromAtten(float targetLightLevel, float accuracy);

	/*
	*  Name:		CalcApproxDistFromAtten()
	*  Parameters:	a float representing the wanted light level, a float representing the desired accuracy of this function,
	*				a float representing the infinite distance, and an unsigned int representing the max number of tries that this function will attempt to get the proper number
	*  Summary:		To get the distance from a light if we want a certain attentuation
	*  Returns:		a float representing the distance away from the light
	*/
	float CalcApproxDistFromAtten(float targetLightLevel, float accuracy, float infiniteDistance, unsigned int maxIterations = DEFAULTMAXITERATIONS);

	/*
	*  Name:		CalcDiffuseFromAttenByDistance()
	*  Parameters:	a float representing the distance from the light, a float representing where the light from the light is essentially zero
	*  Summary:		Calculate the actual amount of light hitting a point
	*  Returns:		float representing the amount of light from a certain distance away
	*/
	float CalcDiffuseFromAttenByDistance(float distance, float zeroThreshold = DEFAULTZEROTHRESHOLD);
};

/*
*  Name:		cLightManager
*  Summary:		Used to manage the lights in the scene
*  Impliments:	nothing
*/
class cLightManager
{
public:
	// ctor
	cLightManager();
	// dtor
	~cLightManager();
	
	// data members
	std::vector<cLight> vecLights;

	/*
	*  Name:		CreateLights()
	*  Parameters:	an int representing the number of lights in the scene, and a bool representing if we should keep the old values of 
	*				the lights while creating new ones
	*  Summary:		Add a new light to the scene
	*  Returns:		nothing
	*/
	void CreateLights(int numberOfLights, bool bKeepOldValues = true);

	/*
	*  Name:		LoadShaderUniformLocations()
	*  Parameters:	an int being the id of the shader we want to use
	*  Summary:		connect the locations of light variables in the shader
	*  Returns:		nothing
	*/
	void LoadShaderUniformLocations(int shaderID);

	/*
	*  Name:		CopyLightInformationToCurrentShader()
	*  Parameters:	nothing
	*  Summary:		Pushes the needed light information to the shader
	*  Returns:		nothing
	*/
	void CopyLightInformationToCurrentShader(void);

};

#endif
