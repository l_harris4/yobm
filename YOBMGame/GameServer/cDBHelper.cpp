#include "cDBHelper.h"

#include <iostream>

cDBHelper::cDBHelper()
{
	return;
}

cDBHelper::~cDBHelper()
{
	return;
}

bool cDBHelper::ConnectToDatabase(const std::string &server,
	const std::string &username,
	const std::string &password,
	const std::string &schema)
{
	try
	{
		driver = get_driver_instance();
		con = driver->connect(server, username, password);
		con->setSchema(schema);
	}
	catch (sql::SQLException &exception)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << exception.what();
		std::cout << " (MySQL error code: " << exception.getErrorCode();
		std::cout << ", SQLState: " << exception.getSQLState() << " )" << std::endl;
		return false;
	}

	return true;
}

bool cDBHelper::Execute(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->execute();
	}
	catch (sql::SQLException &exception)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << exception.what();
		std::cout << " (MySQL error code: " << exception.getErrorCode();
		std::cout << ", SQLState: " << exception.getSQLState() << " )" << std::endl;
		return false;
	}
}

sql::ResultSet* cDBHelper::ExecuteQuery(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->executeQuery();
	}
	catch (sql::SQLException &exception)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << exception.what();
		std::cout << " (MySQL error code: " << exception.getErrorCode();
		std::cout << ", SQLState: " << exception.getSQLState() << " )" << std::endl;
		return false;
	}
}

int cDBHelper::ExecuteUpdate(const std::string &sql)
{
	try
	{
		pstmt = con->prepareStatement(sql);
		return pstmt->executeUpdate();
	}
	catch (sql::SQLException &exception)
	{
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		std::cout << "# ERR: " << exception.what();
		std::cout << " (MySQL error code: " << exception.getErrorCode();
		std::cout << ", SQLState: " << exception.getSQLState() << " )" << std::endl;
		return false;
	}
}