#include "..\dev\gen-cpp\Leaderboard.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <iostream>

#include "cDBHelper.h"
#include <map>
#include <string>
#include <sstream>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

cDBHelper database;

void InitializeWSA()
{
	int iResult = -1;
	WSADATA wsaData;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		exit(-1);
	}
}

class LeaderboardHandler : virtual public LeaderboardIf {
public:
	LeaderboardHandler() {
		// Your initialization goes here
	}

	void setHighScore(const int32_t playerId, const int32_t highScore) {
		// Your implementation goes here
		printf("setHighScore was called!\n");
		try
		{
			std::stringstream exist_sql;
			exist_sql << "SELECT * FROM highscores WHERE player_id = ";
			exist_sql << playerId;
			
			sql::ResultSet* existResult = database.ExecuteQuery(exist_sql.str());
			
			if (existResult == NULL || existResult->rowsCount() == 0)
			{
				// Insert
				std::stringstream insert_sql;
				insert_sql << "INSERT INTO highscores(player_id, highscore) VALUES (";
				insert_sql << playerId;
				insert_sql << ", ";
				insert_sql << highScore;
				insert_sql << ")";
				std::string str = insert_sql.str();
				database.ExecuteUpdate(insert_sql.str());
			}
			else
			{
				// Update
				std::stringstream update_sql;
				update_sql << "UPDATE highscores SET player_id = ";
				update_sql << playerId;
				update_sql << ", highscore = ";
				update_sql << highScore;
				update_sql << " WHERE player_id = ";
				update_sql << playerId;
				database.ExecuteUpdate(update_sql.str());
			}

			//int breakpoint = 0;

		}
		catch (...)
		{
			std::cout << "Something Went Wrong communicating with the database" << std::endl;
		}
	}

	void getTop20(std::map<int32_t, int32_t> & _return) {
		// Your implementation goes here
		printf("getTop20 was called!\n");

		try
		{
			sql::ResultSet* result = database.ExecuteQuery("SELECT * FROM highscores ORDER BY highscore DESC LIMIT 20");
			int numRows = result->rowsCount();
			if (numRows > 0)
			{
				for (int index = 0; index < numRows; index++)
				{
					result->next();
					int player_id = result->getInt("player_id");
					int player_highscore = result->getInt("highscore");

					_return[player_id] = player_highscore;
				}	

				//int breakpoint = 0;
			}
		}
		catch (...)
		{
			std::cout << "Something Went Wrong communicating with the database" << std::endl;
		}

	}

};

int main(int argc, char **argv) 
{
	// Connect to Database 
	database.ConnectToDatabase("127.0.0.1", "root", "root", "yobm_highscores");

	bool done = false;
	int port = 9090;
	shared_ptr<LeaderboardHandler> handler(new LeaderboardHandler());
	shared_ptr<TProcessor> processor(new LeaderboardProcessor(handler));
	shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
	shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
	shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

	// Init WSA 
	InitializeWSA();

	TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
	std::cout << "Server Started: " << std::endl;
	try {
		server.serve();		
	}
	catch (TException& tx) {
		std::cout << "ERROR: " << tx.what() << std::endl;
	}
	system("pause");

	/*while (!done)
	{

	}*/

	return 0;
}